% Load and compare missions
clc
clear all
close all

% Set up platform
figure;
axis([-3000 3000 -3000 3000]);
grid on;
hold on;
plist = [];
cc = hsv(3);
counter = 1;
while (1)
    [x, y, button] = ginput(1);
    if (button ~= 1)
        break;
    end
    [filename, pathname] = uigetfile({'*.route','All Route Files';...
          '*.*','All Files' },'Load Route',...
          '../resource/test_missions/mission1.route');
    plist = csvread(strcat(pathname, filename));
    plot(plist(:,1), plist(:,2), 'color', cc(counter,:));
    counter = counter + 1;
end


