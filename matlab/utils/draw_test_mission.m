% Draw a mission in a platform and see coordinates
clc
clear all
close all

% Set up platform
figure;
axis([-3000 3000 -3000 3000]);
grid on;
hold on;
plist = [];
while (1)
    [x, y, button] = ginput(1);
    if (button ~= 1)
        break;
    end
    plist =[plist; x y 0 50 0 0];
    plot(plist(:,1), plist(:,2));
end

% fprintf('\n Coordinates are: \n');
% for i = 1:size(plist,1)
%     fprintf('%6.2f \t %6.2f\n', plist(i,1), plist(i,2));
% end
[filename, pathname] = uiputfile({'*.route','All Route Files';...
          '*.*','All Files' },'Save Route',...
          '../resource/test_missions/mission1.route');
csvwrite(strcat(pathname, filename), plist);


