function  [ineq_violations,eq_violations] = constraints( vel, vel_boundary, angles, seg_angles, seg_lengths, seg_vel, seg_width, model, arc_fn )
%CONSTRAINTS Summary of this function goes here
%   Let there be n wp. vel is 1x(n-2) wp vel which we are optimizing.
%   vel_boundary is 1x2 boundary velocities, angles is 1x(n-2) angles for
%   the intermediate wp. lengths is 1x(n-1) intersegment lengths. width is
%   1x(n-1) widths for corresponding segments. model is vehicle model.
%   arc_fn is the arc function

length_buffer = zeros(2, length(seg_lengths));
time = 0;

arc_in_tunnel = [];
for i = 1:length(vel)
    vel_pos = max(vel(i), eps);
    [ arc, l1, l2 ] = arc_fn( vel_pos, angles(i), seg_angles(i), seg_angles(i+1),  model );
    arc_in_tunnel = [arc_in_tunnel; amount_arc_in_tunnel( arc, seg_width(i), seg_width(i+1), angles(i))];
    length_buffer(2, i) = l1;
    length_buffer(1, i+1) = l2;
end

vel_total = [vel_boundary(1) vel vel_boundary(2)];
acc_valid = [];
length_violation = [];
for i = 1:length(seg_lengths)
    length_violation = [length_violation;length_buffer(1,i) + length_buffer(2, i) -  seg_lengths(i)];
    acc_length = max(eps, seg_lengths(i) - length_buffer(1,i) - length_buffer(2, i));
    acc_valid = [acc_valid; abs((vel_total(i)^2 - vel_total(i+1)^2)/(2*acc_length)) - model.a_max];
end


%eq_violations=[~arc_in_tunnel];
eq_violations = [];
ineq_violations = [length_violation;
                   acc_valid;
                   arc_in_tunnel];
               
eq_violations = double(eq_violations);

end

