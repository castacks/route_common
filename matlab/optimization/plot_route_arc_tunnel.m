function plot_route_arc_tunnel( coordinates, width, arc_list )
%PLOT_ROUTE_ARC_TUNNEL Summary of this function goes here
%   Detailed explanation goes here
figure;
hold on;
axis equal;
plot(coordinates(1,:), coordinates(2,:), 'b');

dir = normr(diff(coordinates'));
angles = atan2(dir(:,2), dir(:,1));

seg_lengths =  transpose(sqrt(diff(coordinates(1,:)).^2 +  diff(coordinates(2,:)).^2));

for i = 1:length(seg_lengths)
    r = [cos(angles(i)) -sin(angles(i)); sin(angles(i)) cos(angles(i))];
    l1 = seg_lengths(i);
    w1 = width(i);
    box = r*[0 -l1 -l1 0 0; -w1 -w1 w1 w1 -w1];
    box = box + repmat(coordinates(:,i+1), 1, size(box,2));
    plot(box(1,:),box(2,:),'r');
    
    if (i < length(seg_lengths))
        arc = arc_list{i};
        arc = r*arc;
        arc = arc + repmat(coordinates(:,i+1), 1, size(arc,2));
        plot(arc(1,:), arc(2,:),'m');
    end
end

end

