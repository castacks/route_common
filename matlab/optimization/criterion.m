function cost = criterion( vel, vel_boundary, angles, seg_angles, seg_lengths, seg_vel, model, arc_fn)
%COST Summary of this function goes here
%   Let there be n wp. vel is 1x(n-2) wp vel which we are optimizing.
%   vel_boundary is 1x2 boundary velocities, angles is 1x(n-2) angles for
%   the intermediate wp. lengths is 1x(n-1) intersegment lengths. width is
%   1x(n-1) widths for corresponding segments. model is vehicle model.
%   arc_fn is the arc function

length_buffer = zeros(2, length(seg_lengths));
time = 0;

for i = 1:length(vel)
    vel(i) = max(vel(i), eps);
    [ arc, l1, l2 ] = arc_fn( vel(i), angles(i), seg_angles(i), seg_angles(i+1), model );
    arc_length = sum(sqrt(diff(arc(1,:)).^2 + diff(arc(2,:)).^2));
    time = time + arc_length / vel(i);
    length_buffer(2, i) = l1;
    length_buffer(1, i+1) = l2;
end

vel_total = [vel_boundary(1) vel vel_boundary(2)];
for i = 1:length(seg_lengths)
    time = time + get_segment_time( vel_total(i), vel_total(i+1), seg_vel(i), seg_lengths(i) - length_buffer(1,i) - length_buffer(2, i), model);
end

cost = time;

end

