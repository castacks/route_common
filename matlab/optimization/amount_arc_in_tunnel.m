function [ amount ] = amount_arc_in_tunnel( arc, width1, width2, angle )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

l1 = [-1; 0];
l2 = [cos(angle); sin(angle)];

amount = max( min(amount_in_box(arc, l1, width1) , amount_in_box(arc, l2, width2)));
end

function in_box = amount_in_box(p, l, w)
offset = p - l*(l'*p);
in_box =   (l'*p > 0) .* ( offset(1,:).^2 + offset(2,:).^2 - w*w);
end
