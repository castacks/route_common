function [ arc, l1, l2 ] = get_arc_trochoid( v, angle, seg1_angle, seg2_angle, model, wind_speed, wind_direction )
%GET_ARC_TROCHOID Summary of this function goes here
%   Detailed explanation goes here

v = max(v, wind_speed*1.5);
f = get_function_max_curv_wind_heading(model.phi_max, v, wind_speed);

theta = linspace (0, angle, 100);
rel_wind_dir = wrapTo2Pi(seg1_angle + theta - wind_direction);
curv_max = feval(f, rel_wind_dir)';
delta_s = abs(diff(theta))./curv_max(1:(end-1));

arc = [cumsum([0 cos(theta(1:(end-1))).*delta_s]); cumsum([0 sin(theta(1:(end-1))).*delta_s])];

l2 = arc(2,end)/sin(angle);
l1 = arc(1,end) - l2*cos(angle);
arc(1,:) = arc(1,:) - l1;
end

