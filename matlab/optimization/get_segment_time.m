function [time, final_vseg, seg1, seg2, seg3] = get_segment_time( v1, v2, vseg, l, model)
%GET_SEGMENT_TIME Summary of this function goes here
%   Detailed explanation goes here

% take care of illconditioned stuff
v1 = max(v1, 0);
v2 = max(v2, 0);
l = max(l, abs((v2^2 - v1^2)/(2*model.a_max)));
vseg = max(vseg, max(v1, v2));


vlim = sqrt(0.5*(v1^2 + v2^2) + model.a_max*l);

if (vseg < vlim)
    s1 = (vseg^2 - v1^2)/(2*model.a_max);
    s2 = (vseg^2 - v2^2)/(2*model.a_max);
    time = (vseg-v1)/model.a_max + (vseg-v2)/model.a_max + (l-s1-s2)/vseg;
    final_vseg = vseg;
    seg1 = s1; seg2 = (l-s1-s2); seg3 = s2;
else
    time = (vlim-v1)/model.a_max + (vlim-v2)/model.a_max;
    final_vseg = vlim;
    s1 = (vlim^2 - v1^2)/(2*model.a_max);
    s2 = (vlim^2 - v2^2)/(2*model.a_max);
    seg1 = s1; seg2 = 0; seg3 = s2;
end

end

