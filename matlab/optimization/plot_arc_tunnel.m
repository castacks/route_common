function plot_arc_tunnel( arc, l1, l2, w1, w2, angle )
%PLOT_ARC_TUNNEL Summary of this function goes here
%   Detailed explanation goes here

figure;
hold on;
plot(arc(1,:), arc(2,:), 'g');

plot([0 -l1 -l1 0 0],[-w1 -w1 w1 w1 -w1],'r');
 
r = [cos(angle) -sin(angle); sin(angle) cos(angle)];
box2 = r*[0 l2 l2 0 0; w2 w2 -w2 -w2 w2];

plot(box2(1,:),box2(2,:),'r');

axis equal;
end

