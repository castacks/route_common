function [ arc, l1, l2 ]  = get_arc_wind_conservative(  v, angle, seg1_angle, seg2_angle, model, wind_speed, wind_direction )
%GET_ARC_WIND_CONSERVATIVE Summary of this function goes here
%   Detailed explanation goes here

model.phi_max = get_wind_bank_angle(v, wind_speed, model.phi_max);
[ arc, l1, l2 ] = get_arc_simple( v, angle, seg1_angle, seg2_angle, model );
end

