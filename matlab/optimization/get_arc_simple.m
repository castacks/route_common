function [ arc, l1, l2 ] = get_arc_simple( v, angle, seg1_angle, seg2_angle, model )
%GET_ARC_SIMPLE Summary of this function goes here
%   Detailed explanation goes here

r = v^2/(model.g * tan(model.phi_max));
l = r*tan(0.5*angle);
l1 = l;
l2 = l;

gamma = -pi/2 + linspace(0, angle, 30);
arc = [-l + r*cos(gamma); r + r*sin(gamma)];

end

