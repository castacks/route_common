function in_tunnel = is_arc_in_tunnel( arc, width1, width2, angle)
%IS_ARC_IN_TUNNEL Summary of this function goes here
%   width1, width2 are half tunnel widths - the tunnel width on the side of
%   the turn. Also assumes the turn is at 0,0

l1 = [-1; 0];
l2 = [cos(angle); sin(angle)];

in_tunnel = all(is_in_box(arc, l1, width1) | is_in_box(arc, l2, width2));
end

function in_box = is_in_box(p, l, w)
offset = p - l*(l'*p);
in_box = (l'*p > 0) & ( offset(1,:).^2 + offset(2,:).^2 < w*w);
end