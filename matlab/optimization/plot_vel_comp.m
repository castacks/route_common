running_length = 0;
for i = 1:length(seg_lengths)
    [~, optimized_vseg, seg1, seg2, seg3] = get_segment_time( vel_total(i), vel_total(i+1), seg_vel(i), seg_lengths(i) - length_buffer(1,i) - length_buffer(2, i), model);
    % Plot left length
    plot([running_length running_length+length_buffer(1,i)], [vel_total(i) vel_total(i)], 'b');
    %Plot segmenent
    plot([running_length+length_buffer(1,i) running_length+length_buffer(1,i)+seg1 running_length+length_buffer(1,i)+seg1+seg2 running_length+length_buffer(1,i)+seg1+seg2+seg3], [vel_total(i) optimized_vseg optimized_vseg vel_total(i+1)], 'b');
    %Plot right length
    plot([running_length+length_buffer(1,i)+seg1+seg2+seg3 running_length+seg_lengths(i)], [vel_total(i+1) vel_total(i+1)], 'b');
    %Plot upper bound
    plot([running_length running_length+seg_lengths(i)], [seg_vel(i) seg_vel(i)], '--r');

    running_length = running_length + seg_lengths(i);
end
