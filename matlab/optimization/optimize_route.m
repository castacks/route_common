%% Optimize route
clc;
clear;
close all;

%% Load route
plist = csvread('../../resources/mission3.route');

%% Vehicle model
model.g = 9.81;
model.phi_max = deg2rad(30);
model.a_max = 0.075*model.g;

%% Parse the route
dir = normr(diff(plist(:,1:2)));
angles = wrapToPi(diff(atan2(dir(:,2), dir(:,1))));
seg_angles = wrapToPi(atan2(dir(:,2), dir(:,1)));
seg_lengths =  transpose(sqrt(diff(plist(:,1)).^2 +  diff(plist(:,2)).^2));
seg_vel = plist(2:end, 4)';
seg_width = plist(2:end, 5)';
vel_boundary = [plist(1,4) 0];

%% Turn model
%arc_fn = @get_arc_simple;
 
wind_speed = 0.0;
wind_dir = pi/4;
arc_fn = @(  v, angle, seg1_angle, seg2_angle, model ) get_arc_wind_conservative( v, angle, seg1_angle, seg2_angle, model, wind_speed, wind_dir );

% wind_speed = 10.0;
% wind_dir = pi/4;
% arc_fn = @(  v, angle, seg1_angle, seg2_angle, model ) get_arc_wind_circular( v, angle, seg1_angle, seg2_angle, model, wind_speed, wind_dir );

% 
% wind_speed = 10.0;
% wind_dir = pi/4;
% arc_fn = @(  v, angle, seg1_angle, seg2_angle, model ) get_arc_trochoid( v, angle, seg1_angle, seg2_angle, model, wind_speed, wind_dir );


%% Criterion and Cost fn
criterion_fn = @(x) criterion( x, vel_boundary, angles, seg_angles, seg_lengths, seg_vel, model, arc_fn);
constraint_fn = @(x) constraints( x, vel_boundary, angles, seg_angles, seg_lengths, seg_vel, seg_width, model, arc_fn);


%% Parameter vector
p0 = [0.1*seg_vel(2:end)];
lb = [eps*ones(size(p0))];
ub = [seg_vel(2:end)];

% set options for fmincon()
options = optimset('MaxFunEvals',1000000,'Display','iter', 'TolFun', 1e-2, 'MaxIter', 100);

%% do optimization
[p,fval,exitflag]=fmincon(criterion_fn, p0, [],[],[],[],lb, ub, constraint_fn, options);

%% Process results
%Optimized segment velocities
vel = p;

length_buffer = zeros(2, length(seg_lengths));
arc_list = [];
for i = 1:length(vel)
    [ arc, l1, l2 ] = arc_fn( vel(i), angles(i), seg_angles(i), seg_angles(i+1), model );
    length_buffer(2, i) = l1;
    length_buffer(1, i+1) = l2;
    arc_list{i} = arc;
end

vel_total = [vel_boundary(1) vel vel_boundary(2)];
optimized_vseg = [];
for i = 1:length(seg_lengths)
    [~, v] = get_segment_time( vel_total(i), vel_total(i+1), seg_vel(i), seg_lengths(i) - length_buffer(1,i) - length_buffer(2, i), model);
    optimized_vseg = [optimized_vseg v];
end

%% Plot stuff
plot_route_arc_tunnel( plist(:,1:2)', seg_width, arc_list );
plot_vel_profile(vel_total, seg_vel, seg_lengths, length_buffer, model);

%% Total time
fprintf('\n Total time %f \n', fval);