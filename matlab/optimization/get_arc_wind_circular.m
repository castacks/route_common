function [ arc, l1, l2 ] = get_arc_wind_circular(  v, angle, seg1_angle, seg2_angle, model, wind_speed, wind_direction)
%GET_ARC_WIND_CIRCULAR Summary of this function goes here
%   Detailed explanation goes here
v = max(v, wind_speed*1.5);
f = get_function_max_curv_wind_heading(model.phi_max, v, wind_speed);
rel_wind_dir = wrapTo2Pi(linspace (seg1_angle, seg2_angle, 100) - wind_direction);
kappa_min = min(feval(f, rel_wind_dir));
model.phi_max = atan((1/model.g)*(kappa_min*v^2));
[ arc, l1, l2 ] = get_arc_simple( v, angle, seg1_angle, seg2_angle, model );

end

