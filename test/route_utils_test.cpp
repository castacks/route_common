/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * route_utils_test.cpp
 *
 *  Created on: Jan 21, 2015
 *      Author: Sanjiban Choudhury
 */

// Bring in gtest
#include <gtest/gtest.h>
#include <ros/ros.h>
#include <ros/package.h>

#include <route_common/route_utils.h>

using namespace ca;


TEST(IsPointInSingleTunnelTest, cuboid_test) {
  Route route;
  route.waypoints = {RoutePoint(Eigen::Vector3d(0, 0, 0), 0, 0),
                     RoutePoint(Eigen::Vector3d(1e3, 0, 0), 0, 0)};
  route.tunnel = {Tunnel(0, 0, 0, 0),
                  Tunnel(100, 200, 10, 20)};

  ASSERT_TRUE(route_utils::IsPointInSingleTunnel(route, 1, Eigen::Vector3d(500, -80, 0))) << "Should be in left tunnel";
  ASSERT_FALSE(route_utils::IsPointInSingleTunnel(route, 1, Eigen::Vector3d(500, -110, 0))) << "Should NOT be in left tunnel";
  ASSERT_TRUE(route_utils::IsPointInSingleTunnel(route, 1, Eigen::Vector3d(500, 180, 0))) << "Should be in right tunnel";
  ASSERT_FALSE(route_utils::IsPointInSingleTunnel(route, 1, Eigen::Vector3d(500, 230, 0))) << "Should NOT be in right tunnel";
  ASSERT_TRUE(route_utils::IsPointInSingleTunnel(route, 1, Eigen::Vector3d(200, -80, -9))) << "Should be in upper tunnel";
  ASSERT_FALSE(route_utils::IsPointInSingleTunnel(route, 1, Eigen::Vector3d(200, -80, -11))) << "Should NOT be in upper tunnel";
  ASSERT_TRUE(route_utils::IsPointInSingleTunnel(route, 1, Eigen::Vector3d(700, 180, 18))) << "Should be in lower tunnel";
  ASSERT_FALSE(route_utils::IsPointInSingleTunnel(route, 1, Eigen::Vector3d(700, 180, 22))) << "Should NOT be in lower tunnel";
}

TEST(IsPointInSingleTunnelTest, pie_test_1) {
  Route route;
  route.waypoints = {RoutePoint(Eigen::Vector3d(0, 0, 0), 0, 0),
                     RoutePoint(Eigen::Vector3d(1e3, 0, 0), 0, 0),
                     RoutePoint(Eigen::Vector3d(1e3, 1e3, 0), 0, 0)};
  route.tunnel = {Tunnel(0, 0, 0, 0),
                  Tunnel(100, 200, 10, 20),
                  Tunnel(50, 100, 10, 20)};

  ASSERT_TRUE(route_utils::IsPointInSingleTunnel(route, 2, Eigen::Vector3d(1030, -30, 0))) << "Should be in pie";
  ASSERT_FALSE(route_utils::IsPointInSingleTunnel(route, 2, Eigen::Vector3d(1040, -40, 0))) << "Should NOT be in pie";
}

TEST(IsPointInSingleTunnelTest, pie_test_2) {
  Route route;
  route.waypoints = {RoutePoint(Eigen::Vector3d(0, 0, 0), 0, 0),
                     RoutePoint(Eigen::Vector3d(1e3, 0, 0), 0, 0),
                     RoutePoint(Eigen::Vector3d(1e3, 1e3, 0), 0, 0)};
  route.tunnel = {Tunnel(0, 0, 0, 0),
                  Tunnel(100, 200, 10, 20),
                  Tunnel(150, 300, 10, 20)};

  ASSERT_TRUE(route_utils::IsPointInSingleTunnel(route, 2, Eigen::Vector3d(1010, -130, 0))) << "Should be in pie";
  ASSERT_FALSE(route_utils::IsPointInSingleTunnel(route, 2, Eigen::Vector3d(990, -130, 0))) << "Should NOT be in pie";
}

TEST(IsPointInSingleTunnelTest, pie_test_3) {
  Route route;
  route.waypoints = {RoutePoint(Eigen::Vector3d(0, 0, 0), 0, 0),
                     RoutePoint(Eigen::Vector3d(1e3, 0, 0), 0, 0),
                     RoutePoint(Eigen::Vector3d(0, 1e3, 0), 0, 0)};
  route.tunnel = {Tunnel(0, 0, 0, 0),
                  Tunnel(100, 200, 10, 20),
                  Tunnel(150, 300, 10, 20)};

  ASSERT_TRUE(route_utils::IsPointInSingleTunnel(route, 2, Eigen::Vector3d(1149, 0, 0))) << "Should be in pie";
}

// Run all the tests that were declared with TEST()
int main(int argc, char **argv) {
  ros::init(argc, argv, "route_utils_test");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}



