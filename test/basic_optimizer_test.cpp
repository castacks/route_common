/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * basic_optimizer_test.cpp
 *
 *  Created on: Feb 3, 2015
 *      Author: Sanjiban Choudhury
 */

#ifndef ROUTE_COMMON_TEST_BASIC_OPTIMIZER_TEST_CPP_
#define ROUTE_COMMON_TEST_BASIC_OPTIMIZER_TEST_CPP_

// Bring in gtest
#include <gtest/gtest.h>
#include <ros/ros.h>
#include <ros/package.h>

#include <route_common/route_primitives.h>
#include <route_common/basic_route_optimizer.h>

using namespace ca;

TEST(IsTunnelNotModified, right_turn) {
  Route route;
  route.waypoints = {RoutePoint(Eigen::Vector3d(0, 0, 0), 0, 50),
                     RoutePoint(Eigen::Vector3d(4e3, 0, 0), 0, 50),
                     RoutePoint(Eigen::Vector3d(4e3, 4e3, 0), 0, 50)};
  route.tunnel = {Tunnel(0, 0, 0, 0),
                  Tunnel(1e3, 1e3, 200, 200),
                  Tunnel(1e3, 1e3, 200, 200)};
  route.terminal_invariance.type = TerminalInvariance::TI_LOITER;
  route.terminal_invariance.loiter_radius = 800;
  route.terminal_invariance.velocity = 50;

  HelicopterConstraints hc;
  hc.max_velocity = Eigen::Vector3d(50.0, 10.0, 5.0);
  hc.max_accel = 0.075*9.81*Eigen::Vector3d(1,1,1);
  hc.max_roll = M_PI/6;
  hc.max_roll_rate = M_PI/12;
  hc.max_psi_rate = M_PI/6;

  BasicRouteOptimizer::RouteCorrectionParam rcparam;
  rcparam.alpha = 0.3;
  rcparam.breakoff_dist = 2000;

  BasicRouteOptimizer processor;
  processor.set_constraints(hc);
  processor.set_route_correction_param(rcparam);

  RouteOptimizer::EndConstraints ec;
  ec.last_point_stop = false;

  RouteGuide guide;
  processor.ComputeNewRoute(route, ec, guide);

  for (size_t i = 0; i < route.tunnel.size(); i++) {
    EXPECT_FLOAT_EQ(route.tunnel[i].margin_left_m, guide.route.tunnel[i].margin_left_m);
    EXPECT_FLOAT_EQ(route.tunnel[i].margin_right_m, guide.route.tunnel[i].margin_right_m);
  }
}

TEST(IsAsymmetricTunnelUsed, right_turn) {
  Route route;
  route.waypoints = {RoutePoint(Eigen::Vector3d(0, 0, 0), 0, 50),
                     RoutePoint(Eigen::Vector3d(4e3, 0, 0), 0, 50),
                     RoutePoint(Eigen::Vector3d(4e3, 4e3, 0), 0, 50),
  };
  route.tunnel = {Tunnel(1,1,1,1), // garbage tunnel
                  Tunnel(50, 300, 40, 40),
                  Tunnel(50, 300, 40, 40),};
  route.terminal_invariance.type = TerminalInvariance::TI_LOITER;
  route.terminal_invariance.loiter_radius = 800;
  route.terminal_invariance.velocity = 50;


  HelicopterConstraints hc;
  hc.max_velocity = Eigen::Vector3d(50.0, 10.0, 5.0);
  hc.max_accel = 0.075*9.81*Eigen::Vector3d(1,1,1);
  hc.max_roll = M_PI/6;
  hc.max_roll_rate = M_PI/12;
  hc.max_psi_rate = M_PI/6;

  BasicRouteOptimizer::RouteCorrectionParam rcparam;
  rcparam.alpha = 0.3;
  rcparam.breakoff_dist = 2000;

  BasicRouteOptimizer processor;
  processor.set_constraints(hc);
  processor.set_route_correction_param(rcparam);

  RouteOptimizer::EndConstraints ec;
  ec.last_point_stop = false;

  RouteGuide guide;
  processor.ComputeNewRoute(route, ec, guide);


  for (auto it : guide.guide) {
    double l = (it.segment_main.end - it.segment_main.start).norm();
    EXPECT_FLOAT_EQ(50, it.segment_main.speed_profile->SpeedAtDistance(l, l));
  }
}

/*
TEST(IsTerminalInvarianceRespected, high_radius) {
  Route route;
  route.waypoints = {RoutePoint(Eigen::Vector3d(0, 0, 0), 0, 50),
                     RoutePoint(Eigen::Vector3d(4e3, 0, 0), 0, 50),
                     RoutePoint(Eigen::Vector3d(4e3, 4e3, 0), 0, 50),
  };
  route.tunnel = {Tunnel(1,1,1,1), // garbage tunnel
                  Tunnel(50, 300, 40, 40),
                  Tunnel(50, 300, 40, 40),};
  route.terminal_invariance.type = TerminalInvariance::TI_LOITER;
  route.terminal_invariance.loiter_radius = 800;
  route.terminal_invariance.velocity = 50;


  HelicopterConstraints hc;
  hc.max_velocity = Eigen::Vector3d(50.0, 10.0, 5.0);
  hc.max_accel = 0.075*9.81*Eigen::Vector3d(1,1,1);
  hc.max_roll = M_PI/6;
  hc.max_roll_rate = M_PI/12;
  hc.max_psi_rate = M_PI/6;

  BasicRouteOptimizer::RouteCorrectionParam rcparam;
  rcparam.alpha = 0.3;
  rcparam.breakoff_dist = 2000;

  BasicRouteOptimizer processor;
  processor.set_constraints(hc);
  processor.set_route_correction_param(rcparam);

  RouteOptimizer::EndConstraints ec;
  ec.last_point_stop = false;

  RouteGuide guide;
  processor.ComputeNewRoute(route, ec, guide);

  double l = (guide.guide.back().segment_main.end - guide.guide.back().segment_main.start).norm();
  EXPECT_FLOAT_EQ(50, guide.guide.back().segment_main.speed_profile->SpeedAtDistance(l, l));
  EXPECT_FLOAT_EQ(50, guide.route.terminal_invariance.velocity);
  EXPECT_FLOAT_EQ(800, guide.route.terminal_invariance.loiter_radius);
}


TEST(IsTerminalInvarianceRespected, low_radius) {
  Route route;
  route.waypoints = {RoutePoint(Eigen::Vector3d(0, 0, 0), 0, 50),
                     RoutePoint(Eigen::Vector3d(4e3, 0, 0), 0, 50),
                     RoutePoint(Eigen::Vector3d(4e3, 4e3, 0), 0, 50),
  };
  route.tunnel = {Tunnel(1,1,1,1), // garbage tunnel
                  Tunnel(50, 300, 40, 40),
                  Tunnel(50, 300, 40, 40),};
  route.terminal_invariance.type = TerminalInvariance::TI_LOITER;
  route.terminal_invariance.loiter_radius = 400;
  route.terminal_invariance.velocity = 50;


  HelicopterConstraints hc;
  hc.max_velocity = Eigen::Vector3d(50.0, 10.0, 5.0);
  hc.max_accel = 0.075*9.81*Eigen::Vector3d(1,1,1);
  hc.max_roll = M_PI/6;
  hc.max_roll_rate = M_PI/12;
  hc.max_psi_rate = M_PI/6;

  BasicRouteOptimizer::RouteCorrectionParam rcparam;
  rcparam.alpha = 0.3;
  rcparam.breakoff_dist = 2000;

  BasicRouteOptimizer processor;
  processor.set_constraints(hc);
  processor.set_route_correction_param(rcparam);

  RouteOptimizer::EndConstraints ec;
  ec.last_point_stop = false;

  RouteGuide guide;
  processor.ComputeNewRoute(route, ec, guide);

  double l = (guide.guide.back().segment_main.end - guide.guide.back().segment_main.start).norm();
  EXPECT_GT(50, guide.guide.back().segment_main.speed_profile->SpeedAtDistance(l, l));
  EXPECT_GT(50, guide.route.terminal_invariance.velocity);
  EXPECT_FLOAT_EQ(400, guide.route.terminal_invariance.loiter_radius);
}


TEST(IsTerminalInvarianceRespected, low_vel) {
  Route route;
  route.waypoints = {RoutePoint(Eigen::Vector3d(0, 0, 0), 0, 50),
                     RoutePoint(Eigen::Vector3d(4e3, 0, 0), 0, 50),
                     RoutePoint(Eigen::Vector3d(4e3, 4e3, 0), 0, 50),
  };
  route.tunnel = {Tunnel(1,1,1,1), // garbage tunnel
                  Tunnel(50, 300, 40, 40),
                  Tunnel(50, 300, 40, 40),};
  route.terminal_invariance.type = TerminalInvariance::TI_LOITER;
  route.terminal_invariance.loiter_radius = 400;
  route.terminal_invariance.velocity = 30;


  HelicopterConstraints hc;
  hc.max_velocity = Eigen::Vector3d(50.0, 10.0, 5.0);
  hc.max_accel = 0.075*9.81*Eigen::Vector3d(1,1,1);
  hc.max_roll = M_PI/6;
  hc.max_roll_rate = M_PI/12;
  hc.max_psi_rate = M_PI/6;

  BasicRouteOptimizer::RouteCorrectionParam rcparam;
  rcparam.alpha = 0.3;
  rcparam.breakoff_dist = 2000;

  BasicRouteOptimizer processor;
  processor.set_constraints(hc);
  processor.set_route_correction_param(rcparam);

  RouteOptimizer::EndConstraints ec;
  ec.last_point_stop = false;

  RouteGuide guide;
  processor.ComputeNewRoute(route, ec, guide);

  double l = (guide.guide.back().segment_main.end - guide.guide.back().segment_main.start).norm();
  EXPECT_FLOAT_EQ(30, guide.guide.back().segment_main.speed_profile->SpeedAtDistance(l, l));
  EXPECT_FLOAT_EQ(30, guide.route.terminal_invariance.velocity);
  EXPECT_FLOAT_EQ(400, guide.route.terminal_invariance.loiter_radius);
}
*/
// Run all the tests that were declared with TEST()
int main(int argc, char **argv) {
  ros::init(argc, argv, "route_utils_test");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}



#endif  // ROUTE_COMMON_TEST_BASIC_OPTIMIZER_TEST_CPP_ 
