/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * trochoid_arc_generator.h
 *
 *  Created on: Jun 8, 2015
 *      Author: Sanjiban Choudhury
 */

#ifndef ROUTE_COMMON_INCLUDE_ROUTE_COMMON_ARC_GENERATORS_TROCHOID_ARC_GENERATOR_H_
#define ROUTE_COMMON_INCLUDE_ROUTE_COMMON_ARC_GENERATORS_TROCHOID_ARC_GENERATOR_H_

#include "route_common/arc_generators/arc_generator.h"

namespace ca {

class TrochoidArcGenerator : public ArcGenerator {
 public:
  TrochoidArcGenerator() {}
  ~TrochoidArcGenerator() {}

  void GetArc(double vel, double seg1_heading, double seg2_heading, Arc &arc, double &l1, double &l2);

 protected:
  double GetMaxCurveTrochoid(double rel_heading, double heli_speed, double wind_speed, double heli_turnrate);

  double GetTurnRate(double bank_angle, double heli_speed);
};

} // namespace ca



#endif /* ROUTE_COMMON_INCLUDE_ROUTE_COMMON_ARC_GENERATORS_TROCHOID_ARC_GENERATOR_H_ */
