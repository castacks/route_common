/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * arc_generator.h
 *
 *  Created on: Feb 11, 2015
 *      Author: Sanjiban Choudhury
 */

#ifndef ROUTE_COMMON_INCLUDE_ROUTE_COMMON_ARC_GENERATORS_ARC_GENERATOR_H_
#define ROUTE_COMMON_INCLUDE_ROUTE_COMMON_ARC_GENERATORS_ARC_GENERATOR_H_

#include "uav_dynamics_constraints/uav_dynamics_constraints.h"
#include "route_common/route_primitives.h"

namespace ca {

class ArcGenerator {
 public:
  typedef std::vector<Eigen::Vector2d> Arc;

  ArcGenerator()
 : wind_speed_(),
   wind_direction_(),
   hc_() {}

  virtual ~ArcGenerator() {}

  virtual void GetArc(double vel, double seg1_heading, double seg2_heading, Arc &arc, double &l1, double &l2) = 0;

  virtual bool IsArcInTunnel(const Arc &arc, const Tunnel &tunnel1, const Tunnel &tunnel2, double angle);

  virtual double ArcTunnelViolation(const Arc &arc, const Tunnel &tunnel1, const Tunnel &tunnel2, double angle);

  void SetWindData(double wind_speed, double wind_direction) {
    wind_speed_ = wind_speed;
    wind_direction_ = wind_direction;
  }

  void SetConstraints(const UAVDynamicsConstraints hc) {
    hc_ = hc;
  }

 protected:
  bool IsInBox(const Eigen::Vector2d &p, const Eigen::Vector2d &l, double width);
  double AmountInBox(const Eigen::Vector2d &p, const Eigen::Vector2d &l, double width);

  double wind_speed_;
  double wind_direction_;
  UAVDynamicsConstraints hc_;
};

}  // namespace ca



#endif  // ROUTE_COMMON_INCLUDE_ROUTE_COMMON_ARC_GENERATORS_ARC_GENERATOR_H_ 
