/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * route_primitives.h
 *
 *  Created on: Oct 13, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef ROUTE_COMMON_INCLUDE_ROUTE_COMMON_ROUTE_PRIMITIVES_H_
#define ROUTE_COMMON_INCLUDE_ROUTE_COMMON_ROUTE_PRIMITIVES_H_

#include <Eigen/Core>
#include "speed_profile/speed_profile.h"
#include "speed_profile/common_speed_profiles/common_factory.h"

#include "route_common/RoutePoint.h"
#include "route_common/LinePrimitive.h"
#include "route_common/ArcPrimitive.h"
#include "route_common/Tunnel.h"
#include "route_common/Route.h"
#include "route_common/RouteGuide.h"
#include "route_common/RouteMetadata.h"
#include "route_common/RouteSegmentPrimitive.h"
#include "route_common/TerminalInvariance.h"
#include "route_common/TouchdownSite.h"

#include "shapes/shapes.h"

namespace ca {

struct RoutePoint {
  Eigen::Vector3d position;
  double heading;
  double velocity;

  RoutePoint()
  : position(),
    heading(),
    velocity(){}

  RoutePoint(Eigen::Vector3d v1, double v2, double v3)
  : position(v1),
    heading(v2),
    velocity(v3) {}

  route_common::RoutePoint GetMsg() const;
  void SetMsg(const route_common::RoutePoint & msg);
};

/**
 * \brief Line primitive indicates the line section of a route
 */
struct LinePrimitive {
  Eigen::Vector3d start;
  Eigen::Vector3d end;
  SpeedProfile::Ptr speed_profile;

  LinePrimitive():
    start(),
    end(),
    speed_profile(){}

  route_common::LinePrimitive GetMsg(const SpeedProfileFactory::Ptr &speed_prof_factory) const;
  void SetMsg(const route_common::LinePrimitive &msg, const SpeedProfileFactory::Ptr &speed_prof_factory);
};


/**
 * \brief Arc primitive indicates the arc section leading out
 */
struct ArcPrimitive {
  Eigen::Vector3d start;
  Eigen::Vector3d end;
  std::vector<RoutePoint> points;
  SpeedProfile::Ptr speed_profile;


  ArcPrimitive()
 : start(),
   end(),
   points(),
   speed_profile(){}

  route_common::ArcPrimitive GetMsg(const SpeedProfileFactory::Ptr &speed_prof_factory) const;
  void SetMsg(const route_common::ArcPrimitive & msg, const SpeedProfileFactory::Ptr &speed_prof_factory);
};

struct Tunnel {
  double margin_left_m;
  double margin_right_m;
  double margin_up_m;
  double margin_down_m;

  Tunnel()
  : margin_left_m(),
    margin_right_m(),
    margin_up_m(),
    margin_down_m() {}

  Tunnel(double v1, double v2, double v3, double v4)
  : margin_left_m(v1),
    margin_right_m(v2),
    margin_up_m(v3),
    margin_down_m(v4) {}

  route_common::Tunnel GetMsg() const;
  void SetMsg(const route_common::Tunnel & msg);
};

struct TouchdownSite {
  enum ID_TYPE{NON_LANDING=0, FIRST_ID=1};

  int id;
  Eigen::Vector3d position;
  double heading;
  double hover_height;

  route_common::TouchdownSite GetMsg() const;
  void SetMsg(const route_common::TouchdownSite & msg);
};

struct TerminalInvariance {
  enum TERMINAL_INVARIANCE_TYPE{TI_INVALID=0, TI_LAND=1, TI_LOITER=2, TI_HOVER=3};
  unsigned int type; //what type of invariance is it?
  unsigned int segment_id; //where along the route is this invariance (not necessarily end like loiter)

  double loiter_radius;
  double velocity;
  double heading;
  enum ORIENTATION{CLOCKWISE=0, COUNTERCLOCKWISE = 1};
  unsigned int orientation;

  std::vector <TouchdownSite> touchdown_sites;

  PolygonWithHolesDepth endcap;

  TerminalInvariance()
  : type(TI_INVALID),
    segment_id(),
    loiter_radius(),
    velocity(),
    heading(),
    orientation(CLOCKWISE),
    touchdown_sites(),
    endcap() {}

  std::vector<int> GetIDSet() const;
  route_common::TerminalInvariance GetMsg() const;
  void SetMsg(const route_common::TerminalInvariance & msg);
};

struct RouteMetadata {
  unsigned int progress_offset;

  double distance_branchoff;
  double height_branchoff;
  double velocity_branchoff;
  double desired_slope;
  double approach_direction;
  double approach_direction_length;
  double wind_magnitude;
  double wind_direction_from;
  RoutePoint waveoff_detour;

  route_common::RouteMetadata GetMsg() const;
  void SetMsg(const route_common::RouteMetadata & msg);
};

struct RouteSegmentPrimitive {
  LinePrimitive segment_main;
  ArcPrimitive arc_out;

  route_common::RouteSegmentPrimitive GetMsg(const SpeedProfileFactory::Ptr &speed_prof_factory) const;
  void SetMsg(const route_common::RouteSegmentPrimitive & msg, const SpeedProfileFactory::Ptr &speed_prof_factory);
};


struct Route {
  std::vector<RoutePoint> waypoints;
  std::vector<Tunnel> tunnel;

  TerminalInvariance terminal_invariance;

  RouteMetadata metadata;

  route_common::Route GetMsg() const;
  void SetMsg(const route_common::Route & msg);
};


struct RouteGuide {
  Route route;

  std::vector<RouteSegmentPrimitive> guide;
  int guide_index;

  route_common::RouteGuide GetMsg() const;
  void SetMsg(const route_common::RouteGuide & msg);
};


}  // namespace ca


#endif  // ROUTE_COMMON_INCLUDE_ROUTE_COMMON_ROUTE_PRIMITIVES_H_ 
