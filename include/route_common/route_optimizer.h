/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * route_optimizer.h
 *
 *  Created on: Oct 14, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef ROUTE_COMMON_INCLUDE_ROUTE_COMMON_ROUTE_OPTIMIZER_H_
#define ROUTE_COMMON_INCLUDE_ROUTE_COMMON_ROUTE_OPTIMIZER_H_

#include "route_common/route_primitives.h"
#include "uav_dynamics_constraints/uav_dynamics_constraints.h"

namespace ca {

class RouteOptimizer {
public:
  typedef boost::shared_ptr<RouteOptimizer> Ptr;

  struct AdditionalConstraints {
    double dist;
    double height_offset;
    double threshold;
    AdditionalConstraints()
    : dist(),
      height_offset(),
      threshold(){}
  };

  /**
   * \brief This hacky struct represents constraints about the last segment
   */
  struct EndConstraints {
    bool last_point_stop; // is the last point a stop (if this is false, NOTHING ELSE MATTERS)
    bool approach_bottle_neck; // is there an approach bottleneck
    double dist_from_end; // distance of bottleneck from end
    double vel_bottle_neck; //velocity of bottleneck

    AdditionalConstraints additional_constraints;

    SpeedProfile::Ptr approach_profile;
    EndConstraints()
    : last_point_stop(false),
      approach_bottle_neck(false),
      dist_from_end(),
      vel_bottle_neck(),
      approach_profile(),
      additional_constraints(){}
  };


  RouteOptimizer()
  : constraints_() {};

  virtual ~RouteOptimizer(){};

  /**
   * \brief Should be able to initialize of a ros node handle
   * @param n (private node handle in the namespace of the optimizer)
   * @return true if evrything went well
   */

  virtual bool Initialize(ros::NodeHandle &n) = 0;
  /**
   * \brief Compute new route to respect vehicle dynamics
   * Input:
   * input_route - The original route
   * end_constraints - Constraints to be satisfied
   * output_route_guide - The optimized feasible route guide
   * Output:
   * bool: true if successfully computed, false if assumptions broken
   */
  virtual bool ComputeNewRoute(const Route &input_route, const EndConstraints &end_constraints, RouteGuide &output_route_guide) = 0;

  void set_constraints(const UAVDynamicsConstraints &constraints) {constraints_ = constraints;}
protected:
  UAVDynamicsConstraints constraints_;
};

/*
class RouteProcessorMapped : public RouteProcessor {
 public:
  RouteProcessorMapped();
  virtual ~ RouteProcessorMapped();

  virtual bool ComputeNewRoute(const Route &input_route, const RouteCorrectionParam &rcparam, const EndConstraints &end_constraints, Route &output_route);
  virtual bool ComputeProgress(const State &currentState, double &route_progress_index);
  virtual bool ComputeProgressExternal(double route_progress_index, double &external_progress);

  void set_length_threshold_tocompress (double length_threshold_tocompress) {length_threshold_tocompress_ = length_threshold_tocompress;}
  void set_offset_limt_fromcompression (double offset_limt_fromcompression) {offset_limt_fromcompression_ = offset_limt_fromcompression;}

protected:
  std::map<int, int> map_;
  std::map<int, std::pair<int, int> > map_inv_;
  double length_threshold_tocompress_;
  double offset_limt_fromcompression_;

 private:
  bool CompressRoute(const MinimaRoute &input_route,  MinimaRoute &output_route);
};
*/


}  // namespace ca


#endif  // ROUTE_COMMON_INCLUDE_ROUTE_COMMON_ROUTE_OPTIMIZER_H_ 
