/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * time_route_optimizer.h
 *
 *  Created on: Feb 11, 2015
 *      Author: Sanjiban Choudhury
 */

#ifndef ROUTE_COMMON_INCLUDE_ROUTE_COMMON_OPTIMIZERS_TIME_ROUTE_OPTIMIZER_H_
#define ROUTE_COMMON_INCLUDE_ROUTE_COMMON_OPTIMIZERS_TIME_ROUTE_OPTIMIZER_H_

#include "route_common/route_optimizer.h"
#include "route_common/arc_generators/arc_generator.h"

namespace ca {

class TimeRouteOptimizer : public RouteOptimizer {
 public:
  TimeRouteOptimizer()
  : arc_generator_(){}

  virtual ~TimeRouteOptimizer() {}

  virtual bool Initialize(ros::NodeHandle &n) {return true;}

  virtual bool ComputeNewRoute(const Route &input_route, const EndConstraints &end_constraints, RouteGuide &output_route_guide);

  void set_arc_generator(const boost::shared_ptr<ArcGenerator> &arc_generator) { arc_generator_ = arc_generator;}

 protected:
  struct SegmentInfo {
    double length;
    double heading;
    Tunnel tunnel;
    double max_vel;
  };

  struct OptData {
    double v0, vf;
    std::vector<SegmentInfo> segment_set;
    boost::shared_ptr<ArcGenerator> arc_generator;
    UAVDynamicsConstraints hc;
  };

  static double CostFunction(const std::vector<double> &p, std::vector<double> &grad, void *f_data);

  static void ConstraintFunction(unsigned m, double *result, unsigned n, const double* p, double* grad, void* f_data);

  static double GetSegmentTime(double v1, double v2, double vseg, double length, const UAVDynamicsConstraints &hc);

  static double GetSegmentVel(double v1, double v2, double vseg, double length, const UAVDynamicsConstraints &hc);

  boost::shared_ptr<ArcGenerator> arc_generator_;
};

}  // namespace ca



#endif  // ROUTE_COMMON_INCLUDE_ROUTE_COMMON_OPTIMIZERS_TIME_ROUTE_OPTIMIZER_H_ 
