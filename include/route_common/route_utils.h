/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * route_utils.h
 *
 *  Created on: Oct 14, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef ROUTE_COMMON_INCLUDE_ROUTE_COMMON_ROUTE_UTILS_H_
#define ROUTE_COMMON_INCLUDE_ROUTE_COMMON_ROUTE_UTILS_H_

#include "route_common/route_primitives.h"
#include "shapes/shapes.h"

namespace ca {
namespace route_utils {

/**
 * Computes progress along a route using an incremental algorithm
 * @param route
 * @param position
 * @param route_progress_index
 * @return
 */
bool ComputeProgress(const Route &route, const Eigen::Vector3d &position, double &route_progress_index);

bool IsPointInRouteTunnel(const Route &route, const Eigen::Vector3d &position);

bool IsPointInSingleTunnel(const Route &route, unsigned int waypoint_id, const Eigen::Vector3d &position);

ShapeSet ConvertRouteToShapeSet(const Route &route, double margin=10);

ShapeSet ConvertRouteToShapeSetShrunk(const Route &route, double factor);

Polygon ConvertTunnelToPolygon(const Route &route, unsigned int i);

Polygon ConvertRouteToPolygon(const Route &route);

ObliqueRectangularPrism ConvertTunnelToPrism(const Route &route, unsigned int i);

bool LoadRouteFromYAML(const std::string& filename, Route &route);

visualization_msgs::MarkerArray GetMarkerArray(const Route &route);

visualization_msgs::MarkerArray GetMarkerArray(const RouteGuide &guide);

double LocateOnGuide(const RouteGuide &guide, const Eigen::Vector3d &pose, int init_progress);

double ForwardPropagate(const RouteGuide &guide, const Eigen::Vector3d &pose, int current_progress, double distance);

Eigen::Vector3d PositionOnRouteGuide(const RouteGuide &guide, double index);

}
}


#endif  // ROUTE_COMMON_INCLUDE_ROUTE_COMMON_ROUTE_UTILS_H_ 
