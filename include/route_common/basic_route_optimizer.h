/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * basic_route_optimizer.h
 *
 *  Created on: Oct 14, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef ROUTE_COMMON_INCLUDE_ROUTE_COMMON_BASIC_ROUTE_OPTIMIZER_H_
#define ROUTE_COMMON_INCLUDE_ROUTE_COMMON_BASIC_ROUTE_OPTIMIZER_H_

#include "route_common/route_optimizer.h"

namespace ca {

class BasicRouteOptimizer : public RouteOptimizer {
 public:
  struct RouteCorrectionParam {
    //Percentage of segment before which no break off allowed
    double alpha;
    //Distance before which no break off allowed
    double breakoff_dist;

    RouteCorrectionParam():
      alpha(0.1),
      breakoff_dist(200) {}
  };


  BasicRouteOptimizer()
  : route_correction_param_() {}

  virtual ~BasicRouteOptimizer() {}

  virtual bool Initialize(ros::NodeHandle &n);

  virtual bool ComputeNewRoute(const Route &input_route, const EndConstraints &end_constraints, RouteGuide &output_route_guide);

  void set_route_correction_param(const RouteCorrectionParam &route_correction_param) { route_correction_param_ = route_correction_param;}

 protected:
  RouteCorrectionParam route_correction_param_;
};

}  // namespace ca


#endif  // ROUTE_COMMON_INCLUDE_ROUTE_COMMON_BASIC_ROUTE_OPTIMIZER_H_ 
