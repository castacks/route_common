/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_route_processing.cpp
 *
 *  Created on: Oct 14, 2014
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "route_common/route_primitives.h"
#include "route_common/basic_route_optimizer.h"
#include "route_common/RouteGuide.h"
#include "speed_profile/common_speed_profiles/accarray_speed_profile.h"

using namespace ca;

int main(int argc, char **argv) {
  ros::init(argc, argv, "example_route_processing");
  ros::NodeHandle n("~");

  ros::Publisher pub_guide = n.advertise<route_common::RouteGuide>("guide", 5);
  ros::Duration(1.0).sleep();

  Route route;
  {
    RoutePoint rp;
    rp.velocity = 100.0;
    route.waypoints.push_back(rp);

    Tunnel tunnel;
    route.tunnel.push_back(tunnel);
  }

  {
    RoutePoint rp;
    rp.position = Eigen::Vector3d(3000, 0, 100);
    rp.velocity = 50.0;
    route.waypoints.push_back(rp);

    Tunnel tunnel;
    route.tunnel.push_back(tunnel);
  }

  {
    RoutePoint rp;
    rp.position = Eigen::Vector3d(3000, 2000, 200);
    rp.velocity = 50.0;
    route.waypoints.push_back(rp);

    Tunnel tunnel;
    route.tunnel.push_back(tunnel);
  }

  Polygon::VectorEigen2d vertices(4);
  vertices[0] = Eigen::Vector2d(3000-300, 2000+300);
  vertices[1] = Eigen::Vector2d(3000+300, 2000+300);
  vertices[2] = Eigen::Vector2d(3000+300, 2000-300);
  vertices[3] = Eigen::Vector2d(3000-300, 2000-300);

  route.terminal_invariance.endcap.SetOuter(PolygonDepth(vertices, 100, 300));


  RouteOptimizer::EndConstraints ec;
  ec.last_point_stop = true;
  std::vector <double> acc = {0.2, 0.3, 0.4};
  std::vector <double> length =  {0, 100, 400, 600};
  boost::shared_ptr<AccArraySpeedProfile> approach_profile(new AccArraySpeedProfile(0, acc, length, 50.0));
  ec.approach_bottle_neck = true;
  ec.approach_profile = approach_profile;
  ec.dist_from_end = 600;
  ec.vel_bottle_neck = approach_profile->SpeedAtDistance(600,600);

  UAVDynamicsConstraints hc;
  hc.max_accel = Eigen::Vector3d(0.75, 0.75, 0.75);
  hc.max_roll = M_PI/6;
  hc.max_roll_rate = M_PI/12;
  hc.max_velocity = Eigen::Vector3d(50, 10, 5);

  BasicRouteOptimizer::RouteCorrectionParam rcparam;
  rcparam.alpha = 0.3;
  rcparam.breakoff_dist = 2000;

  BasicRouteOptimizer processor;
  processor.set_constraints(hc);
  processor.set_route_correction_param(rcparam);

  RouteGuide guide;
  processor.ComputeNewRoute(route, ec, guide);

  route_common::RouteGuide msg = guide.GetMsg();
  pub_guide.publish(msg);
  ros::Duration(1.0).sleep();
}


