/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * sample_in_tunnel.cpp
 *
 *  Created on: Jan 29, 2015
 *      Author: Sanjiban Choudhury
 */


#include "route_common/route_utils.h"
#include <ros/ros.h>
#include "random_util/random_points.hpp"
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

using namespace ca;

int main(int argc, char **argv) {
  ros::init(argc, argv, "sample_in_tunnel");
  ros::NodeHandle n("~");

  ros::Publisher pub_shape = n.advertise<visualization_msgs::MarkerArray>("tunnel", 1);
  ros::Publisher pub_inliers = n.advertise<PointCloud> ("inliers", 1);

  ros::Duration(1.0).sleep();

  Route route;
  route.waypoints = {RoutePoint(Eigen::Vector3d(100, 100, -50), 0, 0),
      RoutePoint(Eigen::Vector3d(300, 1000, -200), 0, 0),
      RoutePoint(Eigen::Vector3d(-1000, 1200, 0), 0, 0),
      RoutePoint(Eigen::Vector3d(-600, 1800,-100), 0, 0),
  };
  route.tunnel = {Tunnel(10, 20, 30, 40), // garbage tunnel
      Tunnel(50, 100, 10, 20),
      Tunnel(100, 50, 20, 10),
      Tunnel(50, 100, 10, 20)};

  Polygon::VectorEigen2d vertices(4);
  vertices[0] = Eigen::Vector2d(-500, 1900);
  vertices[1] = Eigen::Vector2d(-700, 1900);
  vertices[2] = Eigen::Vector2d(-700, 1700);
  vertices[3] = Eigen::Vector2d(-500, 1700);

  route.terminal_invariance.endcap.SetOuter(PolygonDepth(vertices, -120, -80));

  ShapeSet shape_set = route_utils::ConvertRouteToShapeSet(route);
  pub_shape.publish(shape_set.GetMarkerArray());

  int max_points = 10000;
  int max_trials = 1000000;

  PointCloud::Ptr inlier_msg (new PointCloud);

  ca::UniformVector3d rng;
  rng.ClockSeed();

  int num_trials = 0, num_points = 0;
  while(1) {
    if ((num_trials > max_trials) || (num_points > max_points))
      break;
    Eigen::Vector3d pt = rng();
    pt.x() = 2000*pt.x() - 1500;
    pt.y() = 2000*pt.y();
    pt.z() = 300*pt.z()-250;

    if (shape_set.InShapeSet(std::vector<double>{pt.x(), pt.y(), pt.z()})) {
      num_points ++;
      inlier_msg->points.push_back (pcl::PointXYZ(pt.x(), pt.y(), pt.z()));
    }
    num_trials++;
  }
  inlier_msg->header.frame_id = "world";
  inlier_msg->width = (uint32_t) inlier_msg->points.size();
  inlier_msg->height = 1;
  pub_inliers.publish(inlier_msg);

  ros::Duration(1.0).sleep();
}


