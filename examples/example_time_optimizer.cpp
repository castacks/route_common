/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * example_time_optimizer.cpp
 *
 *  Created on: Feb 12, 2015
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "route_common/route_primitives.h"
#include "route_common/optimizers/time_route_optimizer.h"
#include "route_common/arc_generators/circular_arc_generator.h"
#include "route_common/arc_generators/trochoid_arc_generator.h"
#include "route_common/RouteGuide.h"
#include "speed_profile/common_speed_profiles/accarray_speed_profile.h"
#include "route_common/route_utils.h"

using namespace ca;

int main(int argc, char **argv) {
  ros::init(argc, argv, "example_time_optimizer");
  ros::NodeHandle n("~");

  ros::Publisher pub_guide = n.advertise<route_common::RouteGuide>("guide", 5);
  ros::Publisher pub_guide_ma = n.advertise<visualization_msgs::MarkerArray>("guide_ma", 5);
  ros::Publisher pub_route_ma = n.advertise<visualization_msgs::MarkerArray>("route_ma", 5);
  std::string path;
  if (!n.getParam("route_path", path)) {
    ROS_ERROR_STREAM("Couldnt get route_path");
    return EXIT_FAILURE;
  }
  ros::Duration(1.0).sleep();

  Route route;
  route_utils::LoadRouteFromYAML(path, route);


  RouteOptimizer::EndConstraints ec;
  if (route.terminal_invariance.type == TerminalInvariance::TI_LOITER) {
    ec.last_point_stop = false;
  } else {
    ec.last_point_stop = true;
    std::vector <double> acc = {0.2, 0.3, 0.4};
    std::vector <double> length =  {0, 100, 400, 600};
    boost::shared_ptr<AccArraySpeedProfile> approach_profile(new AccArraySpeedProfile(0, acc, length, 50.0));
    ec.approach_bottle_neck = true;
    ec.approach_profile = approach_profile;
    ec.dist_from_end = 600;
    ec.vel_bottle_neck = approach_profile->SpeedAtDistance(600,600);
  }

  UAVDynamicsConstraints hc;
  hc.max_accel = 9.81*Eigen::Vector3d(0.075, 0.075, 0.075);
  hc.max_roll = M_PI/6;
  hc.max_roll_rate = M_PI/12;
  hc.max_velocity = Eigen::Vector3d(50, 10, 5);


//  CircularArcGenerator circ_arc;
//  circ_arc.SetConstraints(hc);
//  boost::shared_ptr<ArcGenerator> arc_generator(new CircularArcGenerator(circ_arc));

  TrochoidArcGenerator trochoid_arc;
  trochoid_arc.SetConstraints(hc);
  boost::shared_ptr<ArcGenerator> arc_generator(new TrochoidArcGenerator(trochoid_arc));


  TimeRouteOptimizer optimizer;
  optimizer.set_constraints(hc);
  optimizer.set_arc_generator(arc_generator);

  RouteGuide guide;
  bool result = optimizer.ComputeNewRoute(route, ec, guide);

  ROS_ERROR_STREAM("Result "<<result);
  route_common::RouteGuide msg = guide.GetMsg();

  pub_guide.publish(msg);
  pub_guide_ma.publish(route_utils::GetMarkerArray(guide));
  pub_route_ma.publish(route_utils::GetMarkerArray(route));
  ros::Duration(1.0).sleep();
}


