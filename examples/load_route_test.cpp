/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * load_route_test.cpp
 *
 *  Created on: Feb 3, 2015
 *      Author: Sanjiban Choudhury
 */

#include "route_common/route_primitives.h"
#include <iostream>
#include <ros/ros.h>
#include <fstream>
#include "route_common/route_utils.h"
#include <ros/package.h>

using namespace ca;

int main(int argc, char **argv) {
  ros::init(argc, argv, "load_route_test");
  ros::NodeHandle n("~");

  std::string path = ros::package::getPath("route_common") + "/resources/sample_route.bagy";
  Route route;
  route_utils::LoadRouteFromYAML(path, route);
  std::cout<<route.GetMsg()<<"\n";
}



