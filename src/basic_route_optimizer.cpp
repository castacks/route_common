/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * basic_route_optimizer.cpp
 *
 *  Created on: Oct 14, 2014
 *      Author: Sanjiban Choudhury
 */


#include "route_common/basic_route_optimizer.h"

#include <ros/ros.h>
#include <angles/angles.h>

#include "math_utils/math_utils.h"
#include "speed_profile/common_speed_profiles/uniform_speed_profile.h"
#include "speed_profile/common_speed_profiles/trapezoid_speed_profile.h"
#include "speed_profile/common_speed_profiles/accarray_speed_profile.h"
#include "speed_profile/common_speed_profiles/concatenated_speed_profile.h"

namespace ca {

namespace ang = math_utils::angular_math;
namespace nu = math_utils::numeric_operations;
namespace cnst = math_utils::constants;
namespace vm = math_utils::vector_math;

bool BasicRouteOptimizer::Initialize(ros::NodeHandle &n) {
  bool result = true;
  route_correction_param_.alpha = 0.35;
  route_correction_param_.breakoff_dist = 2000.0;
  
  result = result && n.getParam("segment_breakoff_fraction", route_correction_param_.alpha);
  result = result && n.getParam("segment_breakoff_dist", route_correction_param_.breakoff_dist);
  if (!result)
    ROS_WARN_STREAM("Basic Route Optimizer couldnt load parameters");
  return result;
}


bool BasicRouteOptimizer::ComputeNewRoute(const Route &input_route, const EndConstraints &end_constraints, RouteGuide &output_route_guide) {
  //Step 1. Set parameters
  double alpha = route_correction_param_.alpha;
  double breakoff_dist = route_correction_param_.breakoff_dist;

  //Step 2. Check if route makes sense
  /*
  if (input_route.waypoints.size()<=1 || input_route.tunnel.size()<=1 || input_route.waypoints.size()!=input_route.tunnel.size()) {
    ROS_ERROR_STREAM("Route has either not more than 1 way-points / no tunnels / mismatch between way-point and tunnel");
    return false;
  }
  */
  if(input_route.waypoints.size()<=1) {
    ROS_ERROR_STREAM("[basic_route_optimizer] Route has not more than 1 way-points");
    return false;
  }
  if(input_route.tunnel.size()<=1) {
    ROS_ERROR_STREAM("[basic_route_optimizer] Route has no tunnels");
    return false;
  }
  if((input_route.waypoints.size()!=input_route.tunnel.size()) && (2*input_route.waypoints.size()!=input_route.tunnel.size())) {
    ROS_ERROR_STREAM("[basic_route_optimizer] Route has mismatch between way-point and tunnel");
    return false;
  }

  output_route_guide.route = input_route;
  //Step 2a. Compute route accessories to help computation
  //Here we compute the orientation of each leg for segment angle calculation
  for(unsigned int i = 1; i< output_route_guide.route.waypoints.size(); i++)
    output_route_guide.route.waypoints[i].heading = atan2(output_route_guide.route.waypoints[i].position[1]-output_route_guide.route.waypoints[i-1].position[1],
                                                          output_route_guide.route.waypoints[i].position[0]-output_route_guide.route.waypoints[i-1].position[0]);

  output_route_guide.route.waypoints[0].heading = output_route_guide.route.waypoints[1].heading;


  // NEW step
  // We are going to enforce route wide glide slope constraint (in descent)

  /*
   // Commenting out dead code
    *
  if (end_constraints.last_point_stop) {
    double glide_slope = angles::from_degrees(7);

    for(int i=output_route_guide.route.waypoints.size()-2; i >= 0; i--) {
      Eigen::Vector3d dir = output_route_guide.route.waypoints[i+1].position - output_route_guide.route.waypoints[i].position;
      double l = vm::Length2D(dir);
      double delta_z = 0;
      // i and i +1 is the segment
      if (i == output_route_guide.route.waypoints.size()-2) {
        if (l < end_constraints.additional_constraints.dist) {
          ROS_ERROR_STREAM("[basic_route_optimizer] Last segment (approach) is too short. Distance=" << l << " / Expected=" << end_constraints.additional_constraints.dist);
          return false;
        }
        delta_z = (l-end_constraints.additional_constraints.dist)*glide_slope - end_constraints.additional_constraints.threshold + end_constraints.additional_constraints.height_offset;
      } else {
        delta_z = l*glide_slope;
      }

      if (i == 0 && output_route_guide.route.waypoints[i].position.z() < output_route_guide.route.waypoints[i+1].position.z() - delta_z) {
        ROS_ERROR_STREAM("[basic_route_optimizer] First segment glide slope is invalid.");
        return false;
      }

      double margin_down, margin_up;
      if (input_route.waypoints.size() == input_route.tunnel.size()) {
        margin_down = std::min(output_route_guide.route.tunnel[i].margin_down_m, output_route_guide.route.tunnel[i+1].margin_down_m);
        margin_up   = std::min(output_route_guide.route.tunnel[i].margin_up_m,   output_route_guide.route.tunnel[i+1].margin_up_m);
      }
      else {
        margin_down = std::min(output_route_guide.route.tunnel[2*i+1].margin_down_m, output_route_guide.route.tunnel[2*i+2].margin_down_m);
        margin_up   = std::min(output_route_guide.route.tunnel[2*i+1].margin_up_m,   output_route_guide.route.tunnel[2*i+2].margin_up_m);
      }
      
      //Limit value of delta_z if the tunnel margins are not wide enough - use safety margin to keep away from tunnel borders
      if (delta_z > 0.0) {
        if (delta_z > margin_down) {
          ROS_WARN_STREAM("[basic_route_optimizer] Not possible to fix segment " << i << " glide slope. New altitude value is outside flight corridor (lower limit). Limiting value. "<<delta_z <<" "<<margin_down);
          //delta_z = margin_down - 15; //@TODO make the safety margin configurable?
          delta_z = 0.0;
        }
      }
      else {
        if (fabs(delta_z) > margin_up) {
          ROS_WARN_STREAM("[basic_route_optimizer] Not possible to fix segment " << i << " glide slope. New altitude value is outside flight corridor (upper limit). Limiting value."<<delta_z<<" "<<margin_up);
          delta_z = -margin_up + 15; //@TODO make the safety margin configurable?
        }
      }
      //ROS_INFO_STREAM("i=" << i << " delta_z=" << delta_z << " margin_up=" << margin_up << " margin_down=" << margin_down);

      //output_route_guide.route.waypoints[i].position[2] = std::max(output_route_guide.route.waypoints[i].position[2],
      //                                                             output_route_guide.route.waypoints[i+1].position.z() - delta_z);
    }
  }

*/

  //Step 3. Find bound on vertex velocities based on desired segment speeds / vehicle limits / turn constraints
  //Algorithm
  //Reason about segment velocity and vertex velocity
  //First see if the desired segment velocity is achievable within vehicle limits
  //Secondly, see if a lower velocity is required for turning
  // UPDATE: also maintain a vector of turn budget to be used later
  std::vector<double> turn_budget(output_route_guide.route.waypoints.size(), 0.0);

  for(unsigned int i=0; i < output_route_guide.route.waypoints.size(); i++) {
    RoutePoint wp = output_route_guide.route.waypoints[i]; //Current waypoint
    double velocity_bound = 0; //velocity bound of Vertex
    if ( i > 0 && i < output_route_guide.route.waypoints.size()-1) { //not the end points
      //Calculating segment properties
      Eigen::Vector3d dir1 = output_route_guide.route.waypoints[i].position - output_route_guide.route.waypoints[i-1].position;
      Eigen::Vector3d dir2 = output_route_guide.route.waypoints[i+1].position - output_route_guide.route.waypoints[i].position;
      double l1 = dir1.norm();
      if (l1 > 0) dir1.normalize();
      double l2 = dir2.norm();
      if (l2 > 0) dir2.normalize();

      dir1[0] = sqrt(dir1[0]*dir1[0]+dir1[1]*dir1[1]);
      dir1[1] = 0;
      dir2[0] = sqrt(dir2[0]*dir2[0]+dir2[1]*dir2[1]);
      dir2[1] = 0;

      //Sub-algo:
      //Find the segment bounding velocities by reasoning about which factor (vertical / horizontal) is bounding
      //Also compare to actual desired magnitude

      //Segment 1 velocity bound
      double vseg1 = 0, vseg2 = 0;
      if(constraints_.max_velocity[0]*fabs(dir1[2]) > constraints_.max_velocity[2])
        vseg1 = (constraints_.max_velocity[2]/fabs(dir1[2])); //governing factor is vertical vel
      else
        vseg1 = constraints_.max_velocity[0]; //governing factor is planar velocity
      vseg1 = std::min(vseg1, wp.velocity);

      //Segment 2 velocity bound
      if(constraints_.max_velocity[0]*fabs(dir2[2]) > constraints_.max_velocity[2])
        vseg2 = (constraints_.max_velocity[2]/fabs(dir2[2])); //governing factor is vertical velocity
      else
        vseg2 = constraints_.max_velocity[0]; //governing factor is planar velocity
      vseg2 = std::min(vseg2, wp.velocity);

      //We are going to take min(seg1, seg2, turn constraints, tunnel_constraints)
      //Step a. Segment 1
      double h1 = vseg1;
      //Step b. Segment 2
      double h2 = vseg2;
      //Step c. Turn constraints
      double h3 = std::numeric_limits<double>::max(); //to be filled in below
      //Step d. Tunnel constraints
      double h4 = std::numeric_limits<double>::max(); //to be filled in below

      //Sub-algo:
      //Find the maximum velocity that allows a permissible turn
      //Design choice: Find the departure length using alpha and break-off distance
      //Reason about both participating legs

      double lturn1 = std::min(alpha*l1*dir1[0],breakoff_dist); //length budget on left
      double lturn2 = std::min(alpha*l2*dir2[0],breakoff_dist); //length budget on right
      double lmin = std::min(lturn1, lturn2); //minimum of both

      double angle_corner = fabs(ang::WrapToPi(M_PI - (output_route_guide.route.waypoints[i+1].heading - wp.heading)));
      double angle_turn = ang::WrapToPi(output_route_guide.route.waypoints[i+1].heading - wp.heading);
      //We want to solve for horizontal speed
      //Solve the quadratic problem
      //1/(g*tan(phi_max)*tan(0.5*delta_angle)) * v^2  + (phi_max/d_phi_max) * v - lmin = 0
      double qpa, qpb, qpc;
      qpa = 1/(cnst::g*tan(constraints_.max_roll)*tan(std::max(std::numeric_limits<double>::epsilon(),0.5*angle_corner)));
      qpb = constraints_.max_roll/constraints_.max_roll_rate;
      qpc = -lmin;
      h3 = (-qpb + sqrt(qpb*qpb-4*qpa*qpc))/(2*qpa); //take positive solution (guaranteed to be positive because determinant positive)

      double max_width = 0;
      if (input_route.waypoints.size() == input_route.tunnel.size()) {
        //Using symetric tunnel definition
        if (angle_turn >=0 )
          max_width = std::min(output_route_guide.route.tunnel[i].margin_right_m, output_route_guide.route.tunnel[i+1].margin_right_m);
        else
          max_width = std::min(output_route_guide.route.tunnel[i].margin_left_m, output_route_guide.route.tunnel[i+1].margin_left_m);
      }
      else {
        //Using assymetric tunnel defintion - get end of current tunnel and start of next tunnel
        if (angle_turn >=0 )
          max_width = std::min(output_route_guide.route.tunnel[2*i+1].margin_right_m, output_route_guide.route.tunnel[2*i+2].margin_right_m);
        else
          max_width = std::min(output_route_guide.route.tunnel[2*i+1].margin_left_m, output_route_guide.route.tunnel[2*i+2].margin_left_m);
      }

      h4 = sqrt(nu::SafeDiv(max_width*cnst::g*tan(constraints_.max_roll), (1-sin(0.5*angle_corner))));

      //qpa cant be zero by vehicle def
      //velocity_bound = std::min(std::min(h1,h2),h3); //minimum of all bounds
      velocity_bound = std::min(std::min(std::min(h1,h2),h3),h4); //minimum of all bounds

      turn_budget[i] = qpa*velocity_bound*velocity_bound + qpb*velocity_bound;
    } else if(i==0) {
      //For the first point only the first segment matters

      Eigen::Vector3d dir2 = output_route_guide.route.waypoints[i+1].position - output_route_guide.route.waypoints[i].position;
      if (dir2.norm() > 0) dir2.normalize();
      dir2[0] = sqrt(dir2[0]*dir2[0]+dir2[1]*dir2[1]);
      dir2[1] = 0;

      double vseg2 = 0;

      if(constraints_.max_velocity[0]*fabs(dir2[2]) > constraints_.max_velocity[2])
        vseg2 = (constraints_.max_velocity[2]/fabs(dir2[2]));
      else
        vseg2 = constraints_.max_velocity[0];
      vseg2 = std::min(vseg2, wp.velocity);

      velocity_bound = vseg2;
      turn_budget[i] = 0;
    } else if(i==output_route_guide.route.waypoints.size()-1 && !end_constraints.last_point_stop) {
      //For last point only last segment and loiter speed matters if no stop required

      double loiter_vel = output_route_guide.route.terminal_invariance.velocity;

      //Solve the quadratic problem
      //1/(g*tan(phi_max) * v^2  + (phi_max/d_phi_max) * v - lmin = 0
      double qpa, qpb, qpc;
      qpa = 1/(cnst::g*tan(constraints_.max_roll));
      qpb = constraints_.max_roll/constraints_.max_roll_rate;
      qpc = - output_route_guide.route.terminal_invariance.loiter_radius;
      double radius_vel = (-qpb + sqrt(qpb*qpb-4*qpa*qpc))/(2*qpa); //take positive solution (guaranteed to be positive because determinant positive)
      radius_vel = std::numeric_limits<double>::max();

      Eigen::Vector3d dir1 = output_route_guide.route.waypoints[i].position - output_route_guide.route.waypoints[i-1].position;
      if (dir1.norm() > 0) dir1.normalize();

      dir1[0] = sqrt(dir1[0]*dir1[0]+dir1[1]*dir1[1]);
      dir1[1] = 0;

      double vseg1 = 0;
      if(constraints_.max_velocity[0]*fabs(dir1[2]) > constraints_.max_velocity[2])
        vseg1 = (constraints_.max_velocity[2]/fabs(dir1[2]));
      else
        vseg1 = constraints_.max_velocity[0];
      vseg1 = std::min(std::min(std::min(vseg1, wp.velocity), loiter_vel), radius_vel);

      velocity_bound = vseg1;
      turn_budget[i] = 0;
    } else if(i==output_route_guide.route.waypoints.size()-1 && end_constraints.last_point_stop) {
      //Require a stop at the end
      velocity_bound = 0;
      turn_budget[i] = 0;
    }

    if (i==output_route_guide.route.waypoints.size()-2 && end_constraints.last_point_stop && end_constraints.approach_bottle_neck) {
      double l = (output_route_guide.route.waypoints[i].position - output_route_guide.route.waypoints[i+1].position).norm();
      double s = end_constraints.dist_from_end;
      double v = end_constraints.vel_bottle_neck;
      double l_reserved_1 = std::min(turn_budget[i], std::min(alpha*l, breakoff_dist)); // minimum of design buffer or the highest buffer possible
      double l_reserved_2 = std::min(turn_budget[i+1], std::min(alpha*l, breakoff_dist));
      l = l - l_reserved_1 - l_reserved_2 - s; // guaranteed to be > 0

      double v_approach = sqrt(v*v + 2*l*constraints_.max_accel[0]);
      //std::cerr << "V approach "<<v_approach<<" vbound " << velocity_bound<< std::endl;
      velocity_bound = std::min(velocity_bound, v_approach);
    }

    //Updating the route velocity bounds
    output_route_guide.route.waypoints[i].velocity = velocity_bound;
  }

  //Step 4. Iteratively re-evaluate bounds based on acceleration constraints
  //Algorithm:
  //This is a constraint satisfaction problem (CSP)
  //The CSP is simple because constraints are between adjacent vertices
  //Complexity: O(V)
  //Approach: Start with vertex with minimum velocity and propagate acceleration bounds outwards
  //close a vertex once it has been processed.

  std::vector<bool> evaluated; //create vector of evaluated variables (CLOSED LIST)
  for(unsigned int i=0; i<output_route_guide.route.waypoints.size(); i++) {
    evaluated.push_back(false); //initialize list
  }

  bool evaluation_complete = false;
  while(!evaluation_complete) {
    evaluation_complete = true;
    double min_velocity = std::numeric_limits<double>::max();
    int min_idx = -1;
    for(unsigned int i=0; i<output_route_guide.route.waypoints.size(); i++) {
      if(!evaluated[i] && output_route_guide.route.waypoints[i].velocity < min_velocity) {
        evaluation_complete = false;
        min_velocity = output_route_guide.route.waypoints[i].velocity;
        min_idx = i; //pick unevaluated vertex with smallest velocity
      }
    }

    if(evaluation_complete)
      break;

    evaluated[min_idx] = true;
    //Check acceleration on left hand side
    if(min_idx-1>=0) {
      if(!evaluated[min_idx-1]) {
        //Calculate deceleration distance
        double v = output_route_guide.route.waypoints[min_idx-1].velocity;
        double l = (output_route_guide.route.waypoints[min_idx].position - output_route_guide.route.waypoints[min_idx-1].position).norm();
        double l_reserved_1 = std::min(turn_budget[min_idx-1], std::min(alpha*l, breakoff_dist)); // minimum of design buffer or the highest buffer possible
        double l_reserved_2 = std::min(turn_budget[min_idx], std::min(alpha*l, breakoff_dist));
        l = l - l_reserved_1 - l_reserved_2; //subtract off design buffer

        double vmax = sqrt( std::pow(output_route_guide.route.waypoints[min_idx].velocity,2) + 2*constraints_.max_accel[0]*l);
        if(v > vmax && v > 0) {
          //Bound vertex velocity of neighbour
          output_route_guide.route.waypoints[min_idx-1].velocity *= (vmax/v); //no divide by 0
        }
      }
    }

    //Check acceleration on right side
    if(min_idx+1<(int)output_route_guide.route.waypoints.size()) {
      if(!evaluated[min_idx+1]) {
        //Calculate deceleration distance
        double v = output_route_guide.route.waypoints[min_idx+1].velocity;
        double l = (output_route_guide.route.waypoints[min_idx].position - output_route_guide.route.waypoints[min_idx+1].position).norm();
        double l_reserved_1 = std::min(turn_budget[min_idx+1], std::min(alpha*l, breakoff_dist)); // minimum of design buffer or the highest buffer possible
        double l_reserved_2 = std::min(turn_budget[min_idx], std::min(alpha*l, breakoff_dist));
        l = l - l_reserved_1 - l_reserved_2; //subtract off design buffer
        double vmax = sqrt( std::pow(output_route_guide.route.waypoints[min_idx].velocity,2) + 2*constraints_.max_accel[0]*l);
        if(v>vmax && v>0) {
          //Bound vertex velocity of neighbour
          output_route_guide.route.waypoints[min_idx+1].velocity *= (vmax/v); //no divide by 0
        }
      }
    }
  }


  //Note output_route_guide.route.waypoints still contains vertex velocities

  //TODO make the route guide calculation generic
  //Step 4.5
  //Calculate turn guides

  output_route_guide.guide_index = 0;
  output_route_guide.guide.resize(output_route_guide.route.waypoints.size() - 1);

  output_route_guide.guide.front().segment_main.start = output_route_guide.route.waypoints.front().position;
  output_route_guide.guide.back().segment_main.end = output_route_guide.route.waypoints.back().position;
  output_route_guide.guide.back().arc_out.speed_profile.reset(new UniformSpeedProfile(0));

  // the last arc has no significance

  for(unsigned int i=1; i<output_route_guide.route.waypoints.size()-1; i++) {
    //We calculate the arc primitive for
    //i -1 arc_out
    //i arc in
    double angle = fabs(ang::WrapToPi(M_PI - (output_route_guide.route.waypoints[i+1].heading - output_route_guide.route.waypoints[i].heading)));
    double vel =  output_route_guide.route.waypoints[i].velocity;
    double length = vel*vel/(cnst::g*tan(constraints_.max_roll)*tan(std::max(std::numeric_limits<double>::epsilon(),0.5*angle))) + vel*(constraints_.max_roll/constraints_.max_roll_rate);
    Eigen::Vector3d dir1 = output_route_guide.route.waypoints[i].position - output_route_guide.route.waypoints[i-1].position;
    Eigen::Vector3d dir2 = output_route_guide.route.waypoints[i+1].position - output_route_guide.route.waypoints[i].position;
    if (dir1.norm() > 0) dir1.normalize();
    if (dir2.norm() > 0) dir2.normalize();
    Eigen::Vector3d bisec = vm::Length2D(dir1)*dir2 - vm::Length2D(dir2)*dir1; //formula ||u||v + ||v||u
    bisec[2] = 0;
    if (bisec.norm() > 0) bisec.normalize();

    RoutePoint start,end;
    start.position = output_route_guide.route.waypoints[i].position - (length/sqrt(dir1[0]*dir1[0]+dir1[1]*dir1[1]+std::numeric_limits<double>::epsilon()))*dir1;
    start.heading = output_route_guide.route.waypoints[i].heading;
    start.velocity = vel;
    end.position = output_route_guide.route.waypoints[i].position + (length/sqrt(dir2[0]*dir2[0]+dir2[1]*dir2[1]+std::numeric_limits<double>::epsilon()))*dir2;
    //end.position[2] = start.position[2]; // make turns flat
    end.heading = output_route_guide.route.waypoints[i+1].heading;
    end.velocity = vel;

    ArcPrimitive turn;
    turn.start = start.position;
    turn.end = end.position;

    int no_seg = 12;
    double radius = length*tan(std::max(std::numeric_limits<double>::epsilon(),0.5*angle));
    Eigen::Vector3d centre = output_route_guide.route.waypoints[i].position + bisec*sqrt(length*length+radius*radius);
    centre[2] = 0;
    double offset = atan2(start.position[1] - centre[1], start.position[0] - centre[0]);
    double span = ang::WrapToPi(atan2(end.position[1]-centre[1], end.position[0] - centre[0]) - offset);
    if(radius > 1e-3 && radius < 1e6) {
      for(int j=0; j<=no_seg; j++) {
        double fraction = ((double)j/(double)no_seg);
        double curr_ang = offset + fraction*span;
        Eigen::Vector3d pos = centre + radius*Eigen::Vector3d(cos(curr_ang), sin(curr_ang), 0.0);
        pos[2] = ((no_seg-j)*start.position[2] + j*end.position[2])/(no_seg);

        RoutePoint arc_point;
        arc_point.position = pos;
        arc_point.heading = ang::WrapToPi(start.heading + fraction*span);
        arc_point.velocity = vel;
        turn.points.push_back(arc_point);
      }
    } else if (radius >= 1e6) {
      for(int j=0; j<=no_seg; j++) {
        double fraction = ((double)j/(double)no_seg);
        Eigen::Vector3d pos = ((no_seg-j)*start.position + j*end.position)/(no_seg);

        RoutePoint arc_point;
        arc_point.position = pos;
        arc_point.heading = ang::WrapToPi(start.heading);
        arc_point.velocity = vel;
        turn.points.push_back(arc_point);
      }
    } else {
      turn.points.push_back(start);
      turn.points.push_back(end);
    }

    turn.speed_profile.reset(new UniformSpeedProfile(vel));

    //i-1 arc out
    output_route_guide.guide[i-1].arc_out = turn;

    //i-1 end segment location
    output_route_guide.guide[i-1].segment_main.end = turn.start;
    //i start segment location
    output_route_guide.guide[i].segment_main.start = turn.end;
  }


  //Step 5. Evaluate max achievable segment speeds / tunnel and do analysis book-keeping
  //Algorithm:
  //Since we have vertex velocities, we attempt to see what is the maximum peak segment
  //velocity we can reach. Of course, we limit this to the desired velocity from the
  //original route
  //Using golden formula: If v1 is velocity of p1 and v2 of p2, the peak velocity possible is
  // vpeak = sqrt( (v1^2 + v2^2)/2 + a*l)
  // above is easy to derive

  bool is_route_same = true; //did we override the original route?
  Route temp_route = output_route_guide.route; //make a local copy
  for(unsigned int i = 0; i < output_route_guide.route.waypoints.size(); i++) {
    double orig_des_vel = input_route.waypoints[i].velocity; //original velocity of the route
    if (i > 0) {
      //First re-calculate bound of segment velocity
      double v1 = output_route_guide.route.waypoints[i-1].velocity;
      double v2 = output_route_guide.route.waypoints[i].velocity;
      Eigen::Vector3d dir = output_route_guide.route.waypoints[i].position - output_route_guide.route.waypoints[i-1].position;
      double l = dir.norm();
      if (l > 0) dir.normalize();

      double l_reserved_1 = std::min(turn_budget[i-1], std::min(alpha*l, breakoff_dist)); // minimum of design buffer or the highest buffer possible
      double l_reserved_2 = std::min(turn_budget[i], std::min(alpha*l, breakoff_dist));
      l = l - l_reserved_1 - l_reserved_2; //subtract off design buffer
      dir[0] = sqrt(dir[0]*dir[0]+dir[1]*dir[1]);
      dir[1] = 0;
      double vseg=0;
      if(constraints_.max_velocity[0]*fabs(dir[2]) > constraints_.max_velocity[2])
        vseg = (constraints_.max_velocity[2]/fabs(dir[2]));
      else
        vseg = constraints_.max_velocity[0];

      vseg = std::min(vseg, orig_des_vel);

      //Apply golden formula to see what the peak bound is
      temp_route.waypoints[i].velocity = std::min(vseg, sqrt(0.5*(v1*v1+v2*v2) + constraints_.max_accel[0]*l));
    }


    if(temp_route.waypoints[i].velocity < orig_des_vel && is_route_same) {
      //If recomputation is different from original - report!
      is_route_same = false;
    }

    /*
    if(i>0 && i<output_route_guide.route.waypoints.size()-1) {
      //Calculate tunnel
      //Sub algo:
      // Calculate deviation from the CORNER of the segment intersection
      // Inflate by a buffer indicating size of vehicle

      double angle_corner = fabs(ang::WrapToPi(M_PI - (output_route_guide.route.waypoints[i+1].heading - output_route_guide.route.waypoints[i].heading)));
      double vel = output_route_guide.route.waypoints[i].velocity;
      double dev  = (vel*vel)/(cnst::g*tan(constraints_.max_roll)*sin(std::max(std::numeric_limits<double>::epsilon(),0.5*angle_corner)));
      double tunnel_width = 2.0 * dev ; //+ 2*(model_->mc.BUFFERSIZE); //width is twice deviation and buffer
      //Design choice for height:
      //Since we are not explicitly stating our height following algorithm
      //we use a conservative bound of the worst case vertical velocity change
      //this may be too pessimistic - please modify as wish
      double tunnel_height = 2.0 * 2.0 * (constraints_.max_velocity[2]*constraints_.max_velocity[2]) / (2*constraints_.max_accel[2]); //height is twice conservative bound
      temp_route.tunnel[i].margin_left_m = std::max(tunnel_width, output_route_guide.route.tunnel[i].margin_left_m);
      temp_route.tunnel[i].margin_right_m = std::max(tunnel_width, output_route_guide.route.tunnel[i].margin_right_m);
      temp_route.tunnel[i].margin_up_m = std::max(tunnel_height, output_route_guide.route.tunnel[i].margin_up_m);
      temp_route.tunnel[i].margin_down_m = std::max(tunnel_height, output_route_guide.route.tunnel[i].margin_down_m);

      if(is_route_same && (temp_route.tunnel[i].margin_left_m >  output_route_guide.route.tunnel[i].margin_left_m
          || temp_route.tunnel[i].margin_right_m >  output_route_guide.route.tunnel[i].margin_right_m
          || temp_route.tunnel[i].margin_up_m >  output_route_guide.route.tunnel[i].margin_up_m
          || temp_route.tunnel[i].margin_down_m >  output_route_guide.route.tunnel[i].margin_down_m))
        is_route_same = false;
    }*/
  }

  // output_route_guide.route = temp_route;  Commenting this because i need both temp route velocities (segment vel) and waypoitn vel (output_route_guide)

  //TODO make this generic as well
  //Step 5.5
  //Calculate velocity guides

  for(unsigned int i = 0; i < output_route_guide.guide.size(); i++) {
    if (i == output_route_guide.guide.size() - 1 && end_constraints.approach_bottle_neck && end_constraints.last_point_stop) {
      double length = (output_route_guide.guide[i].segment_main.end - output_route_guide.guide[i].segment_main.start).norm();
      length -= end_constraints.dist_from_end;
      double v1 = output_route_guide.route.waypoints[i].velocity;
      double v2 = end_constraints.vel_bottle_neck;
      double vel = temp_route.waypoints[i+1].velocity;
      vel = std::min(vel, sqrt(0.5*(v1*v1+v2*v2) + constraints_.max_accel[0]*length));
      temp_route.waypoints[i+1].velocity = vel;

      double a1 = vel < v1 ? -constraints_.max_accel[0] : constraints_.max_accel[0]; //a1 has to be positive always
      double a3 = vel < v2 ? constraints_.max_accel[0] : -constraints_.max_accel[0];

      TrapezoidSpeedProfile trap_spf(v1,vel,v2,a1,a3);
      boost::shared_ptr<AccArraySpeedProfile> acc_spf = boost::dynamic_pointer_cast<AccArraySpeedProfile>(end_constraints.approach_profile);

      output_route_guide.guide[i].segment_main.speed_profile.reset(new ConcatenatedSpeedProfile(trap_spf, *acc_spf));

    } else {
      double vel = temp_route.waypoints[i+1].velocity;
      double v1 = output_route_guide.route.waypoints[i].velocity;
      double v3 = output_route_guide.route.waypoints[i+1].velocity;

      double a1 = vel < v1 ? -constraints_.max_accel[0] : constraints_.max_accel[0]; //a1 has to be positive always
      double a3 = vel < v3 ? constraints_.max_accel[0] : -constraints_.max_accel[0];
      output_route_guide.guide[i].segment_main.speed_profile.reset(new TrapezoidSpeedProfile(v1, vel, v3, a1, a3));
    }
  }

  /*
  for (unsigned int i = 0; i < output_route_guide.route.waypoints.size(); i++) {
    output_route_guide.route.tunnel[i] = temp_route.tunnel[i];
    output_route_guide.route.tunnel[i] = temp_route.tunnel[i];
  }*/

  //Step 6. Post process to fix up tunnels
  /*  if(output_route_guide.route.waypoints.size() >2) {
    //output_route_guide.route.tunnel[0] = output_route_guide.route.tunnel[1];
    output_route_guide.route.tunnel[output_route_guide.route.waypoints.size()-1] =  output_route_guide.route.tunnel[output_route_guide.route.waypoints.size()-2];
  }
   */
  /* why was this code active?? doesnt this always blow up tunnels to max
  for(unsigned int i=1; i<output_route_guide.route.waypoints.size(); i++) {
    //Tunnel on both sides of a turn should be symmetric to capture a turn fully
    temp_route.tunnel[i] = Eigen::Vector3d( std::max(output_route_guide.route.tunnel[i-1].margin_left_m, output_route_guide.route.tunnel[i][0]) , std::max(output_route_guide.route.tunnel[i-1][1], output_route_guide.route.tunnel[i][1]), 0 );
  }*/

  /*
  // Post process it to fix up invariances for loiter
  //Step 5: Post-process the route to correct for starting configuration
  //If the vehicle started in the tunnel, its tricky to know if it can follow the route
  //Case A: Its going slower than it should => its guaranteed to follow the route
  //Case B: Its gong faster than it should - the only option then is to do a full turn in place till it can decelerate
  //For A and B both we will use the conservative approach: inflate tunnel for full turn
  //If vehicle started outside tunnel do the same inflation

  double vel = math_tools::Length(query_state.rates.velocity_mps);
  double min_radius = vel*vel/(math_consts::GRAVITY*tan(model_->mc.BANKANGLE_LIMIT));
  double slack_length = vel*(model_->mc.BANKANGLE_LIMIT/model_->mc.BANKANGLERATE_LIMIT);
  double min_tunnel_width = 2*(2*min_radius+slack_length+model_->mc.BUFFERSIZE);
  Vector3D old_tunnel = input_route.tunnel[0];
  input_route.tunnel[0] = Vector3D(std::max(min_tunnel_width,old_tunnel[0]),old_tunnel[1],0.0);

   */

  //Step 6: Post-process to set invariance and correct loiter radius (if any)
  //Note: For multiple loiter points, only first will be corrected
  //If there is no stop state, we will throw an error
  // if(input_mission.type==aacus_msgs::MissionCommand::MT_LAND) {

  // Should endcap be increased??

  // Get the correct speed to do loiter from the shape profile
  // Radius WILL NOT BE changed


  if(output_route_guide.route.terminal_invariance.type == TerminalInvariance::TI_LOITER) {


    double vel = output_route_guide.route.waypoints.back().velocity;
    double radius = output_route_guide.route.terminal_invariance.loiter_radius;  // (FIXME) this doesnt seem right
    //Correct loiter radius
    if(radius < (vel*vel)/(cnst::g*tan(0.9*constraints_.max_roll)) +  vel*constraints_.max_roll/constraints_.max_roll_rate)
      radius = (vel*vel)/(cnst::g*tan(0.9*constraints_.max_roll)) + vel* constraints_.max_roll/constraints_.max_roll_rate;
    output_route_guide.route.terminal_invariance.loiter_radius = radius;

   //output_route_guide.route.terminal_invariance.velocity = output_route_guide.route.waypoints.back().velocity;
  }

  // FIXME Why isnt temp route velocities pushed in?
  for (unsigned int i = 0; i < temp_route.waypoints.size(); i++) {
    output_route_guide.route.waypoints[i].velocity = temp_route.waypoints[i].velocity;
  }

  return true;
}

}  // namespace ca

