/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * route_utils.cpp
 *
 *  Created on: Oct 14, 2014
 *      Author: Sanjiban Choudhury
 */

#include "route_common/route_utils.h"
#include <ros/ros.h>
#include "math_utils/math_utils.h"
#include "boost/math/constants/constants.hpp"
#include "std_msgs_util/std_msgs_util.h"
#include "shapes/shape_utils.h"
#include <fstream>
#include <yaml-cpp/yaml.h>


using namespace YAML;

namespace YAML {

template<> struct convert<Eigen::Vector3d> {
  static Node encode(const Eigen::Vector3d& rhs) {
    Node node;
    node["x"] = rhs.x();
    node["y"] = rhs.y();
    node["z"] = rhs.z();
    return node;
  }

  static bool decode(const Node& node, Eigen::Vector3d& rhs) {
    rhs[0] = node["x"].as<double>();
    rhs[1] = node["y"].as<double>();
    rhs[2] = node["z"].as<double>();
    return true;
  }
};


template<> struct convert<ca::RoutePoint> {
  static Node encode(const ca::RoutePoint& rhs) {
    Node node;
    node["position"] = rhs.position;
    node["heading"] = rhs.heading;
    node["velocity"] = rhs.velocity;
    return node;
  }

  static bool decode(const Node& node, ca::RoutePoint& rhs) {
    rhs.position = node["position"].as<Eigen::Vector3d>();
    rhs.heading = node["heading"].as<double>();
    rhs.velocity = node["velocity"].as<double>();
    return true;
  }
};


template<> struct convert<ca::Tunnel> {
  static Node encode(const ca::Tunnel& rhs) {
    Node node;
    node["margin_left_m"] = rhs.margin_left_m;
    node["margin_right_m"] = rhs.margin_right_m;
    node["margin_up_m"] = rhs.margin_up_m;
    node["margin_down_m"] = rhs.margin_down_m;
    return node;
  }

  static bool decode(const Node& node, ca::Tunnel& rhs) {
    rhs.margin_left_m = node["margin_left_m"].as<double>();
    rhs.margin_right_m = node["margin_right_m"].as<double>();
    rhs.margin_up_m = node["margin_up_m"].as<double>();
    rhs.margin_down_m = node["margin_down_m"].as<double>();
    return true;
  }
};

template<> struct convert<ca::TouchdownSite> {
  static Node encode(const ca::TouchdownSite& rhs) {
    Node node;
    node["id"] = rhs.id;
    node["position"] = rhs.position;
    node["heading"] = rhs.heading;
    node["hover_height"] = rhs.hover_height;
    return node;
  }

  static bool decode(const Node& node, ca::TouchdownSite& rhs) {
    rhs.id = node["id"].as<int>();
    rhs.position = node["position"].as<Eigen::Vector3d>();
    rhs.heading = node["heading"].as<double>();
    rhs.hover_height = node["hover_height"].as<double>();
    return true;
  }
};


template<> struct convert<ca::TerminalInvariance> {
  static Node encode(const ca::TerminalInvariance& rhs) {
    Node node;
    node["type"] = rhs.type;
    node["segment_id"] = rhs.segment_id;
    node["loiter_radius"] = rhs.loiter_radius;
    node["velocity"] = rhs.velocity;
    node["heading"] = rhs.heading;
    node["orientation"] = rhs.orientation;
    for (size_t i = 0; i < rhs.touchdown_sites.size(); i++)
      node["touchdown_sites"][i] = rhs.touchdown_sites[i];
    node["endcap"] = rhs.endcap;
    return node;
  }

  static bool decode(const Node& node, ca::TerminalInvariance& rhs) {
    rhs.type = node["type"].as<int>();
    rhs.segment_id = node["segment_id"].as<int>();
    rhs.loiter_radius = node["loiter_radius"].as<double>();
    rhs.velocity = node["velocity"].as<double>();
    rhs.heading = node["heading"].as<double>();
    rhs.orientation = node["orientation"].as<double>();
    for (size_t i = 0; i < node["touchdown_sites"].size(); i++)
      rhs.touchdown_sites.push_back(node["touchdown_sites"][i].as<ca::TouchdownSite>());
    rhs.endcap = node["endcap"].as<ca::PolygonWithHolesDepth>();
    return true;
  }
};

template<> struct convert<ca::RouteMetadata> {
  static Node encode(const ca::RouteMetadata& rhs) {
    Node node;
    node["progress_offset"] = rhs.progress_offset;
    node["distance_branchoff"] = rhs.distance_branchoff;
    node["height_branchoff"] = rhs.height_branchoff;
    node["velocity_branchoff"] = rhs.velocity_branchoff;
    node["desired_slope"] = rhs.desired_slope;
    node["approach_direction"] = rhs.approach_direction;
    node["approach_direction_length"] = rhs.approach_direction_length;
    node["wind_magnitude"] = rhs.wind_magnitude;
    node["wind_direction_from"] = rhs.wind_direction_from;
    node["waveoff_detour"] = rhs.waveoff_detour;
    return node;
  }

  static bool decode(const Node& node, ca::RouteMetadata& rhs) {
    rhs.progress_offset = node["progress_offset"].as<int>();
    rhs.distance_branchoff = node["distance_branchoff"].as<double>();
    rhs.height_branchoff = node["height_branchoff"].as<double>();
    rhs.velocity_branchoff = node["velocity_branchoff"].as<double>();
    rhs.desired_slope = node["desired_slope"].as<double>();
    rhs.approach_direction = node["approach_direction"].as<double>();
    rhs.approach_direction_length = node["approach_direction_length"].as<double>();
    rhs.wind_magnitude = node["wind_magnitude"].as<double>();
    rhs.wind_direction_from = node["wind_direction_from"].as<double>();
    rhs.waveoff_detour = node["waveoff_detour"].as<ca::RoutePoint>();
    return true;
  }
};

template<> struct convert<ca::Route> {
  static Node encode(const ca::Route& route) {
    Node node;
    Node rp_nodes, tunnel_nodes;

    for (size_t i = 0; i < route.waypoints.size(); i++)
      rp_nodes[i] = route.waypoints[i];

    for (size_t i = 0; i < route.tunnel.size(); i++)
      tunnel_nodes[i] = route.waypoints[i];

    node["waypoints"] = rp_nodes;
    node["tunnel"] = tunnel_nodes;
    node["terminal_invariance"] = route.terminal_invariance;
    node["metadata"] = route.metadata;
    return node;
  }

  static bool decode(const Node& node, ca::Route& route) {
    const YAML::Node& rp_nodes = node["waypoints"];
    for(size_t i = 0; i < rp_nodes.size(); i++) {
      ca::RoutePoint rp;
      rp = rp_nodes[i].as<ca::RoutePoint>();
      route.waypoints.push_back(rp);
    }

    const YAML::Node& tunnel_nodes = node["tunnel"];
    for(size_t i = 0; i < tunnel_nodes.size(); i++) {
      ca::Tunnel tun;
      tun = tunnel_nodes[i].as<ca::Tunnel>();
      route.tunnel.push_back(tun);
    }
    route.terminal_invariance = node["terminal_invariance"].as<ca::TerminalInvariance>();
    route.metadata = node["metadata"].as<ca::RouteMetadata>();
    return true;
  }
};
}

namespace ca {
namespace route_utils {

namespace ang = math_utils::angular_math;
namespace cnst = math_utils::constants;
namespace vm = math_utils::vector_math;
namespace nu = math_utils::numeric_operations;

bool ComputeProgress(const Route &route, const Eigen::Vector3d &position, double &route_progress_index) {

/*
  if(route.waypoints.size()<=1 || route.tunnel.size()<=1 || route.waypoints.size()!=route.tunnel.size()) {
    ROS_ERROR_STREAM("Route has either not more than 1 way-points / no tunnels / mismatch between way-point and tunnel");
    return false;
  }
*/

  if(route.waypoints.size()<=1) {
    ROS_ERROR_STREAM("[route_utils] Route has not more than 1 way-points");
    return false;
  }
  if(route.tunnel.size()<=1) {
    ROS_ERROR_STREAM("[route_utils] Route has no tunnels");
    return false;
  }
  if(route.waypoints.size()!=route.tunnel.size()) {
    ROS_ERROR_STREAM("[route_utils] Route has mismatch between way-point and tunnel");
    return false;
  }

  int current_progress = floor(route_progress_index);
  double prev_progress = route_progress_index;
  //Current progress can't be negative - making it 0
  if(current_progress < 0)
    current_progress = 0;

  // New case: runaway vehicle.
  // If vehicle is closer to another segment other than 1 or 2, then just set current progress to that one.
  double min_dis = std::numeric_limits<double>::max();
  int min_dis_idx = current_progress;
  for (int candidate = current_progress; candidate < (int)route.waypoints.size() - 1; candidate++) {
    double temp;
    if (vm::VectorToLineSegment(route.waypoints[candidate].position, route.waypoints[candidate+1].position, position, temp).norm() < min_dis) {
      min_dis = vm::VectorToLineSegment(route.waypoints[candidate].position, route.waypoints[candidate+1].position, position, temp).norm();
      min_dis_idx = candidate;
    }
  }

  if (min_dis_idx > current_progress + 1)
    current_progress = min_dis_idx;

  if (current_progress >= (int) route.waypoints.size() - 1) {
    //Case 1. Beyond end point
    route_progress_index = route.waypoints.size()-1;
  }
  else if (current_progress == (int) route.waypoints.size() - 2) {
    //Case 2 - In the last leg
    bool  onEnd=false;
    double rv=-1;
    vm::VectorToLineSegment(route.waypoints[current_progress].position, route.waypoints[current_progress+1].position, position, rv);
    if(rv>1.0) {
      route_progress_index = current_progress+1;
    } else {
      if(rv<0)
        rv=0;
      route_progress_index = current_progress+rv;
    }
  } else if (current_progress < (int)route.waypoints.size() - 2) {
    //Case 3. Consider segment 1 and 2 and try to decide where vehicle is
    bool  onEnd=false;
    double rv1=-1, rv2=-1;
    Eigen::Vector3d vec1 = vm::VectorToLineSegment(route.waypoints[current_progress].position, route.waypoints[current_progress+1].position, position, rv1);
    Eigen::Vector3d vec2 = vm::VectorToLineSegment(route.waypoints[current_progress+1].position, route.waypoints[current_progress+2].position, position, rv2);

    if (rv1 < 0) {
      //Case 2a. Before segment 1
      route_progress_index = current_progress + 0.0;
    } else if((rv1>=0 && rv1<=1 && rv2<0) || (rv1>=0 && rv1<=1 && rv2>1)) {
      //Case 2b. In segment 1 and before segment 2 OR In segment 1 and after segment 2
      route_progress_index = current_progress + rv1;
    } else if((rv1>=0 && rv1<=1 && rv2>=0 && rv2<=1) || (rv1>1 && rv2<0) ) {
      //Case 2c. In segment 1 and segment2 OR beyond segment 1 and before segment 2
      if(vec1.norm() < vec2.norm()) {
        //Closer to 1
        if(rv1>1)
          rv1 = 1;
        route_progress_index = current_progress + rv1;
      } else {
        //Closer to 2
        if(rv2<0)
          rv2=0;
        route_progress_index = current_progress + 1 + rv2;
      }

    } else if(rv1>1 && rv2>=0 && rv2<=1) {
      //Case 2e. After segment 1 and in segment 2
      route_progress_index = current_progress + 1 + rv2;
    } else {
      ROS_ERROR_STREAM("Logic error in progress calculation (rv1: "<<rv1<<" rv2:" <<rv2);
    }
  }

  route_progress_index = std::max(route_progress_index, prev_progress);
  return true;
}


double LocateOnGuide(const RouteGuide &guide, const Eigen::Vector3d &pose, int init_progress) {
  double rv = 0;
  int current_progress = init_progress;
  vm::VectorToLineSegment(guide.guide[current_progress].segment_main.start, guide.guide[current_progress].segment_main.end, pose, rv);
  rv = std::max(0.0, std::min(1.0, rv));
  return rv + current_progress;
}


double ForwardPropagate(const RouteGuide &guide, const Eigen::Vector3d &pose, int init_progress, double distance) {
  int current_progress = init_progress;
  double start_index = LocateOnGuide(guide, pose, init_progress);
  // Case 1: Its the end
  if (current_progress  >= guide.guide.size())
    return start_index;

  double alpha = start_index - current_progress;

  double cum_distance = (guide.guide[current_progress].segment_main.start - guide.guide[current_progress].segment_main.end).norm() * (1 - alpha);

  // Case 2: Its on the same line

  if (distance < cum_distance)
    return start_index + distance / (guide.guide[current_progress].segment_main.start - guide.guide[current_progress].segment_main.end).norm();

  // Case 3: Its on some ither line
  for (std::size_t i = 1; i < guide.guide[current_progress].arc_out.points.size(); i++)
    cum_distance += (guide.guide[current_progress].arc_out.points[i].position - guide.guide[current_progress].arc_out.points[i-1].position).norm();

  current_progress +=1;

  while (cum_distance < distance && current_progress < guide.guide.size()) {
    double length = (guide.guide[current_progress].segment_main.start - guide.guide[current_progress].segment_main.end).norm();
    if (cum_distance + length >= distance)
      return current_progress + (distance - cum_distance)/ length;
    cum_distance += length;
    for (std::size_t i = 1; i < guide.guide[current_progress].arc_out.points.size(); i++)
      cum_distance += (guide.guide[current_progress].arc_out.points[i].position - guide.guide[current_progress].arc_out.points[i-1].position).norm();
    current_progress += 1;
  }

  return current_progress;
}


Eigen::Vector3d PositionOnRouteGuide(const RouteGuide &guide, double index) {
  int segment = floor(index);
  // Case 1: Its the end
  if (segment  >= guide.guide.size())
    return guide.guide.back().segment_main.end;

  double alpha = index - segment;

  return (1-alpha)*guide.guide[segment].segment_main.start + alpha*guide.guide[segment].segment_main.end;
}


bool IsPointInRouteTunnel(const Route &route, const Eigen::Vector3d &position) {
  ShapeSet shape_set = ConvertRouteToShapeSet(route);
  return shape_set.InShapeSet(std::vector<double> {position.x(), position.y(), position.z()});
}

bool IsPointInSingleTunnel(const Route &route, unsigned int waypoint_id, const Eigen::Vector3d &position) {
  if (waypoint_id == 0 || route.waypoints.size() <= 1)
    return false;
  ShapeSet shape_set;

  int i = waypoint_id;
  Eigen::Vector3d pt1, pt2;
  pt1 = route.waypoints[i-1].position;
  pt2 = route.waypoints[i].position;
  Tunnel tunnel = route.tunnel[i];

  if (i == 1)
    pt1 = pt2 - (pt2-pt1)*nu::SafeDiv((pt2-pt1).norm() + tunnel.margin_left_m + tunnel.margin_right_m,(pt2-pt1).norm());

  if (i>=2) {
    Eigen::Vector3d pt_prev = route.waypoints[i-2].position;
    Eigen::Vector2d dir1(pt1.x()-pt_prev.x(), pt1.y()-pt_prev.y()), dir2(pt2.x()-pt1.x(), pt2.y()-pt1.y());
    double x = pt1.x(), y = pt1.y(), radius, theta_start, theta_end;
    double lower_z = pt1.z() - tunnel.margin_up_m;
    double upper_z = pt1.z() + tunnel.margin_down_m;
    if ( (dir1.x()*dir2.y() - dir1.y()*dir2.x()) > 0 ) {
      radius = tunnel.margin_left_m;
      theta_start = atan2(dir1.y(), dir1.x()) - boost::math::constants::pi<double>()/2.0;
      theta_end = atan2(dir2.y(), dir2.x()) - boost::math::constants::pi<double>()/2.0;
    } else {
      radius = tunnel.margin_right_m;
      theta_start = atan2(dir2.y(), dir2.x()) + boost::math::constants::pi<double>()/2.0;
      theta_end = atan2(dir1.y(), dir1.x()) + boost::math::constants::pi<double>()/2.0;
    }
    shape_set.AddShape(ShapePtr(new SectorDepth(x,y,radius,theta_start, theta_end, lower_z, upper_z)));
  }
  ObliqueRectangularPrism prism(pt1, pt2, tunnel.margin_right_m, tunnel.margin_left_m, tunnel.margin_down_m, tunnel.margin_up_m);
  shape_set.AddShape(ShapePtr(new ObliqueRectangularPrism(prism)));

  if (waypoint_id == route.waypoints.size() - 1)
    shape_set.AddShape(ShapePtr(new PolygonWithHolesDepth(route.terminal_invariance.endcap)));

  return shape_set.InShapeSet(std::vector<double> {position.x(), position.y(), position.z()});
}

ShapeSet ConvertRouteToShapeSet(const Route &route, double margin) {
  ShapeSet shape_set;
  if (route.waypoints.size() == 0)
    return shape_set;

  for (unsigned int i = 1; i < route.waypoints.size(); i++) {
    Eigen::Vector3d pt1, pt2;
    pt1 = route.waypoints[i-1].position;
    pt2 = route.waypoints[i].position;
    Tunnel tunnel = route.tunnel[i];
    tunnel.margin_up_m += margin;
    tunnel.margin_down_m += margin;
    tunnel.margin_left_m += margin;
    tunnel.margin_right_m += margin;
    if (i == 1)
      pt1 = pt2 - (pt2-pt1)*nu::SafeDiv((pt2-pt1).norm() + tunnel.margin_left_m + tunnel.margin_right_m,(pt2-pt1).norm());

    if (i>=2) {
      Eigen::Vector3d pt_prev = route.waypoints[i-2].position;
      Eigen::Vector2d dir1(pt1.x()-pt_prev.x(), pt1.y()-pt_prev.y()), dir2(pt2.x()-pt1.x(), pt2.y()-pt1.y());
      double x = pt1.x(), y = pt1.y(), radius, theta_start, theta_end;
      double lower_z = pt1.z() - tunnel.margin_up_m;
      double upper_z = pt1.z() + tunnel.margin_down_m;
      if ( (dir1.x()*dir2.y() - dir1.y()*dir2.x()) > 0 ) {
        radius = tunnel.margin_left_m;
        theta_start = atan2(dir1.y(), dir1.x()) - boost::math::constants::pi<double>()/2.0;
        theta_end = atan2(dir2.y(), dir2.x()) - boost::math::constants::pi<double>()/2.0;
      } else {
        radius = tunnel.margin_right_m;
        theta_start = atan2(dir2.y(), dir2.x()) + boost::math::constants::pi<double>()/2.0;
        theta_end = atan2(dir1.y(), dir1.x()) + boost::math::constants::pi<double>()/2.0;
      }
      shape_set.AddShape(ShapePtr(new SectorDepth(x,y,radius,theta_start, theta_end, lower_z, upper_z)));
    }
    ObliqueRectangularPrism prism(pt1, pt2, tunnel.margin_right_m, tunnel.margin_left_m, tunnel.margin_down_m, tunnel.margin_up_m);
    shape_set.AddShape(ShapePtr(new ObliqueRectangularPrism(prism)));
  }

  shape_set.AddShape(ShapePtr(new PolygonWithHolesDepth(route.terminal_invariance.endcap)));

  return shape_set;
}

ShapeSet ConvertRouteToShapeSetShrunk(const Route &route, double factor) {
  ShapeSet shape_set;
  for (unsigned int i = 1; i < route.waypoints.size(); i++) {
    Eigen::Vector3d pt1, pt2;
    pt1 = route.waypoints[i-1].position;
    pt2 = route.waypoints[i].position;
    Tunnel tunnel = route.tunnel[i];

    if (i == 1)
      pt1 = pt2 - (pt2-pt1)*nu::SafeDiv((pt2-pt1).norm() + tunnel.margin_left_m + tunnel.margin_right_m,(pt2-pt1).norm());

    if (i>=2) {
      Eigen::Vector3d pt_prev = route.waypoints[i-2].position;
      Eigen::Vector2d dir1(pt1.x()-pt_prev.x(), pt1.y()-pt_prev.y()), dir2(pt2.x()-pt1.x(), pt2.y()-pt1.y());
      double x = pt1.x(), y = pt1.y(), radius, theta_start, theta_end;
      double lower_z = pt1.z() - tunnel.margin_up_m;
      double upper_z = pt1.z() + tunnel.margin_down_m;
      if ( (dir1.x()*dir2.y() - dir1.y()*dir2.x()) > 0 ) {
        radius = tunnel.margin_left_m;
        theta_start = atan2(dir1.y(), dir1.x()) - boost::math::constants::pi<double>()/2.0;
        theta_end = atan2(dir2.y(), dir2.x()) - boost::math::constants::pi<double>()/2.0;
      } else {
        radius = tunnel.margin_right_m;
        theta_start = atan2(dir2.y(), dir2.x()) + boost::math::constants::pi<double>()/2.0;
        theta_end = atan2(dir1.y(), dir1.x()) + boost::math::constants::pi<double>()/2.0;
      }
      shape_set.AddShape(ShapePtr(new SectorDepth(x,y,radius,theta_start, theta_end, lower_z, upper_z)));
    }
    ObliqueRectangularPrism prism(pt1, pt2, tunnel.margin_right_m*factor, tunnel.margin_left_m*factor, tunnel.margin_down_m*factor, tunnel.margin_up_m*factor);
    shape_set.AddShape(ShapePtr(new ObliqueRectangularPrism(prism)));
  }

  shape_set.AddShape(ShapePtr(new PolygonWithHolesDepth(route.terminal_invariance.endcap)));

  return shape_set;
}

Polygon ConvertTunnelToPolygon(const Route &route, unsigned int i) {
  if (i >= route.waypoints.size() - 1)
    return Polygon();

  Eigen::Vector3d pos1, pos2;
  pos1 = route.waypoints[i].position;
  pos2 = route.waypoints[i+1].position;
  Tunnel tunnel = route.tunnel[i+1];

  if (i == 0)
    pos1 = pos2 - (pos2-pos1)*nu::SafeDiv((pos2-pos1).norm() + tunnel.margin_left_m + tunnel.margin_right_m,(pos2-pos1).norm());


  Eigen::Vector3d new_pos2 = pos1 + (pos2-pos1)*nu::SafeDiv((pos2-pos1).norm() + 1,(pos2-pos1).norm());
  Eigen::Vector3d new_pos1 = pos2 - (pos2-pos1)*nu::SafeDiv((pos2-pos1).norm() + 1,(pos2-pos1).norm());

  Eigen::Vector2d dir(new_pos2.x() - new_pos1.x(), new_pos2.y() - new_pos1.y());
  dir.normalize();
  Eigen::Vector2d dir_right(-dir.y(), dir.x());

  Polygon::VectorEigen2d vertices(4);
  vertices[0] = Eigen::Vector2d(new_pos1.x(),new_pos1.y()) + tunnel.margin_right_m*dir_right;
  vertices[1] = Eigen::Vector2d(new_pos2.x(),new_pos2.y()) + tunnel.margin_right_m*dir_right;
  vertices[2] = Eigen::Vector2d(new_pos2.x(),new_pos2.y()) - tunnel.margin_left_m*dir_right;
  vertices[3] = Eigen::Vector2d(new_pos1.x(),new_pos1.y()) - tunnel.margin_left_m*dir_right;
  Polygon valid_area(vertices);

  return valid_area;
}


Polygon ConvertRouteToPolygon(const Route &route) {
    if (route.waypoints.size() == 0) {
        return Polygon();
    }

    Polygon valid_area = ConvertTunnelToPolygon(route, 0);
    for (unsigned int i=1; i<(route.waypoints.size()-1); i++) {
        Polygon extra_area = ConvertTunnelToPolygon(route, i);
        if (!ca::shape_utils::JoinPolygon(valid_area, extra_area, valid_area)) {
            ROS_ERROR_STREAM("Segment: Initial, couldnt stich visibility polygons - tunnel segment " << i);
        }        
    }
    /*
    for (unsigned int i=2; i<route.waypoints.size(); i++) {
      //Get Sector information: position x y, radius, angle start / end
      Tunnel tunnel = route.tunnel[i];
      Eigen::Vector3d pt0 = route.waypoints[i-2].position;
      Eigen::Vector3d pt1 = route.waypoints[i-1].position;
      Eigen::Vector3d pt2 = route.waypoints[i].position;
      Eigen::Vector2d dir1(pt1.x()-pt0.x(), pt1.y()-pt0.y()), dir2(pt2.x()-pt1.x(), pt2.y()-pt1.y());
      double x = pt1.x(), y = pt1.y(), radius, theta_start, theta_end;
      if ( (dir1.x()*dir2.y() - dir1.y()*dir2.x()) > 0 ) {
        radius = tunnel.margin_left_m;
        theta_start = atan2(dir1.y(), dir1.x()) - boost::math::constants::pi<double>()/2.0;
        theta_end = atan2(dir2.y(), dir2.x()) - boost::math::constants::pi<double>()/2.0;
      } else {
        radius = tunnel.margin_right_m;
        theta_start = atan2(dir2.y(), dir2.x()) + boost::math::constants::pi<double>()/2.0;
        theta_end = atan2(dir1.y(), dir1.x()) + boost::math::constants::pi<double>()/2.0;
      }
      
      //Use sector information to create a polygon: circular section discret. 5 degrees angle increments
      double angleInc = 5.0*(M_PI/180.0);
      int numSteps = (int)(theta_end - theta_start)/angleInc;
      Polygon::VectorEigen2d vertices(numSteps + 2);
      for (int i=0; i<numSteps; i++) {
        vertices[i] = Eigen::Vector2d(x + radius*cos(theta_start + i*angleInc) , y+radius*sin(theta_start + i*angleInc));
      }
      vertices[numSteps] = Eigen::Vector2d(x + radius*cos(theta_end) , y+radius*sin(theta_end));
      vertices[numSteps+1] = Eigen::Vector2d(x, y);
      Polygon extra_area(vertices);
      if (!ca::shape_utils::JoinPolygon(valid_area, extra_area, valid_area)) {
        ROS_ERROR_STREAM("Segment: Initial, couldnt stich visibility polygons - sector segment " << i);
      }        
    }
    */
    return valid_area;
}


ObliqueRectangularPrism ConvertTunnelToPrism(const Route &route, unsigned int i) {
  if (i >= route.waypoints.size() - 1)
    return ObliqueRectangularPrism(Eigen::Vector3d::Zero(), Eigen::Vector3d::Zero(), 0 ,0 , 0 ,0);

  Eigen::Vector3d pos1, pos2;
  pos1 = route.waypoints[i].position;
  pos2 = route.waypoints[i+1].position;
  Tunnel tunnel = route.tunnel[i+1];

  if (i == 0)
    pos1 = pos2 - (pos2-pos1)*nu::SafeDiv((pos2-pos1).norm() + tunnel.margin_left_m + tunnel.margin_right_m,(pos2-pos1).norm());

  ObliqueRectangularPrism prism(pos1, pos2, tunnel.margin_right_m, tunnel.margin_left_m, tunnel.margin_down_m, tunnel.margin_up_m);
  return prism;
}


bool LoadRouteFromYAML(const std::string& filename, Route &route) {
  try {
    std::ifstream fin(filename.c_str());
    YAML::Node doc = YAML::Load(fin);
    route = doc.as<ca::Route>();
  } catch(YAML::ParserException& e) {
    ROS_ERROR_STREAM("YAML loading exception "<<e.what());
    return false;
  }
  return true;
}

visualization_msgs::MarkerArray GetMarkerArray(const Route &route) {
  visualization_msgs::MarkerArray ma;
  ShapeSet shape_set = route_utils::ConvertRouteToShapeSet(route);
  visualization_msgs::MarkerArray ma_shape_set = shape_set.GetMarkerArray();
  for (auto it : ma_shape_set.markers)
    ma.markers.push_back(it);

  visualization_msgs::Marker m_wp;
  m_wp.header.frame_id = "/world";
  m_wp.header.stamp = ros::Time::now();
  m_wp.ns = "wp";
  m_wp.type = visualization_msgs::Marker::SPHERE_LIST;
  m_wp.action = visualization_msgs::Marker::ADD;
  m_wp.pose.position.x = 1;
  m_wp.pose.position.y = 1;
  m_wp.pose.position.z = 1;
  m_wp.pose.orientation.x = 0.0;
  m_wp.pose.orientation.y = 0.0;
  m_wp.pose.orientation.z = 0.0;
  m_wp.pose.orientation.w = 1.0;
  m_wp.color.r = 1.0; m_wp.color.b = 0.0; m_wp.color.g = 0.4; m_wp.color.a = 0.8;
  m_wp.scale.x = 3.1; m_wp.scale.y = 3.1; m_wp.scale.z = 3.1;


  visualization_msgs::Marker m_seg;
  m_seg.header.frame_id = "/world";
  m_seg.header.stamp = ros::Time::now();
  m_seg.ns = "seg";
  m_seg.type = visualization_msgs::Marker::LINE_STRIP;
  m_seg.action = visualization_msgs::Marker::ADD;
  m_seg.pose.position.x = 1;
  m_seg.pose.position.y = 1;
  m_seg.pose.position.z = 1;
  m_seg.pose.orientation.x = 0.0;
  m_seg.pose.orientation.y = 0.0;
  m_seg.pose.orientation.z = 0.0;
  m_seg.pose.orientation.w = 1.0;
  m_seg.color.r = 1.0; m_seg.color.b = 0.0; m_seg.color.g = 0.4; m_seg.color.a = 0.2;
  m_seg.scale.x = 2.0; m_seg.scale.y = 2.0; m_seg.scale.z = 2.0;

  for (auto it : route.waypoints) {
    geometry_msgs::Point pt;
    pt.x = it.position.x(); pt.y = it.position.y(); pt.z = it.position.z();
    m_wp.points.push_back(pt);
    m_seg.points.push_back(pt);
  }

  ma.markers.push_back(m_wp);
  ma.markers.push_back(m_seg);
  return ma;
}

visualization_msgs::MarkerArray GetMarkerArray(const RouteGuide &guide) {
  visualization_msgs::MarkerArray ma;

  visualization_msgs::Marker m_seg;
  m_seg.header.frame_id = "/world";
  m_seg.header.stamp = ros::Time::now();
  m_seg.ns = "wp";
  m_seg.type = visualization_msgs::Marker::LINE_LIST;
  m_seg.action = visualization_msgs::Marker::ADD;
  m_seg.pose.position.x = 1;
  m_seg.pose.position.y = 1;
  m_seg.pose.position.z = 1;
  m_seg.pose.orientation.x = 0.0;
  m_seg.pose.orientation.y = 0.0;
  m_seg.pose.orientation.z = 0.0;
  m_seg.pose.orientation.w = 1.0;
  m_seg.color.r = 1.0; m_seg.color.b = 0.0; m_seg.color.g = 0.0; m_seg.color.a = 0.9;
  m_seg.scale.x = 2.0; m_seg.scale.y = 2.0; m_seg.scale.z = 2.0;

  visualization_msgs::Marker m_arc;
  m_arc.header.frame_id = "/world";
  m_arc.header.stamp = ros::Time::now();
  m_arc.ns = "arc";
  m_arc.type = visualization_msgs::Marker::LINE_LIST;
  m_arc.action = visualization_msgs::Marker::ADD;
  m_arc.pose.position.x = 1;
  m_arc.pose.position.y = 1;
  m_arc.pose.position.z = 1;
  m_arc.pose.orientation.x = 0.0;
  m_arc.pose.orientation.y = 0.0;
  m_arc.pose.orientation.z = 0.0;
  m_arc.pose.orientation.w = 1.0;
  m_arc.color.r = 0.0; m_arc.color.b = 0.0; m_arc.color.g = 1.0; m_arc.color.a = 0.9;
  m_arc.scale.x = 2.0; m_arc.scale.y = 2.0; m_arc.scale.z = 2.0;

  visualization_msgs::Marker m_route;
  m_route.header.frame_id = "/world";
  m_route.header.stamp = ros::Time::now();
  m_route.ns = "rp";
  m_route.type = visualization_msgs::Marker::LINE_LIST;
  m_route.action = visualization_msgs::Marker::ADD;
  m_route.pose.position.x = 1;
  m_route.pose.position.y = 1;
  m_route.pose.position.z = 1;
  m_route.pose.orientation.x = 0.0;
  m_route.pose.orientation.y = 0.0;
  m_route.pose.orientation.z = 0.0;
  m_route.pose.orientation.w = 1.0;
  m_route.color.r = 0.5; m_route.color.b = 1.0; m_route.color.g = 0.0; m_route.color.a = 0.9;
  m_route.scale.x = 2.0; m_route.scale.y = 2.0; m_route.scale.z = 2.0;


  bool first = true;
  RoutePoint prev_point;
  for(auto it: guide.route.waypoints)
  {
    if(first)
    {
      first = false;
      prev_point = it;
      continue;
    }
    geometry_msgs::Point pt1, pt2;
    pt1.x = prev_point.position.x(); pt1.y = prev_point.position.y(); pt1.z = prev_point.position.z();
    pt2.x = it.position.x(); pt2.y = it.position.y(); pt2.z = it.position.z();
    m_route.points.push_back(pt1);
    m_route.points.push_back(pt2);
    prev_point = it;
  }

  for (auto it : guide.guide) {
    geometry_msgs::Point pt1, pt2;
    pt1.x = it.segment_main.start.x(); pt1.y = it.segment_main.start.y(); pt1.z = it.segment_main.start.z();
    pt2.x = it.segment_main.end.x(); pt2.y = it.segment_main.end.y(); pt2.z = it.segment_main.end.z();
    m_seg.points.push_back(pt1);
    m_seg.points.push_back(pt2);
    if (it.arc_out.points.size() == 0)
      continue;
    for (size_t i = 0; i < (it.arc_out.points.size() - 1); i++) {
      geometry_msgs::Point pt1, pt2;
      pt1.x = it.arc_out.points[i].position.x(); pt1.y = it.arc_out.points[i].position.y(); pt1.z = it.arc_out.points[i].position.z();
      pt2.x = it.arc_out.points[i+1].position.x(); pt2.y = it.arc_out.points[i+1].position.y(); pt2.z = it.arc_out.points[i+1].position.z();
      m_arc.points.push_back(pt1);
      m_arc.points.push_back(pt2);
    }
  }

  ma.markers.push_back(m_arc);
  ma.markers.push_back(m_seg);
  ma.markers.push_back(m_route);

  return ma;
}

}  // namespace route_utils
}  // namespace ca




