/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * route_primitive.cpp
 *
 *  Created on: Oct 13, 2014
 *      Author: Sanjiban Choudhury
 */

#include "route_common/route_primitives.h"
#include "geom_cast/point_cast.hpp"

namespace ca {

route_common::RoutePoint  RoutePoint::GetMsg() const {
  route_common::RoutePoint msg;
  msg.position = ca::point_cast<geometry_msgs::Point>(position);
  msg.heading = heading;
  msg.velocity = velocity;
  return msg;
}

void  RoutePoint::SetMsg(const route_common::RoutePoint & msg) {
  position = ca::point_cast<Eigen::Vector3d>(msg.position);
  heading = msg.heading;
  velocity = msg.velocity;
}


route_common::LinePrimitive LinePrimitive::GetMsg(const SpeedProfileFactory::Ptr &speed_prof_factory) const {
  route_common::LinePrimitive msg;
  msg.start = ca::point_cast<geometry_msgs::Point>(start);
  msg.end = ca::point_cast<geometry_msgs::Point>(end);
  msg.speed_profile = speed_prof_factory->Get(speed_profile);
  return msg;
}


void LinePrimitive::SetMsg(const route_common::LinePrimitive &msg, const SpeedProfileFactory::Ptr &speed_prof_factory) {
  start = ca::point_cast<Eigen::Vector3d>(msg.start);
  end = ca::point_cast<Eigen::Vector3d>(msg.end);
  speed_profile.reset(speed_prof_factory->Get(msg.speed_profile));
}

route_common::ArcPrimitive ArcPrimitive::GetMsg(const SpeedProfileFactory::Ptr &speed_prof_factory) const {
  route_common::ArcPrimitive msg;
  msg.start = ca::point_cast<geometry_msgs::Point>(start);
  msg.end = ca::point_cast<geometry_msgs::Point>(end);
  for (auto& it : points)
    msg.points.push_back(it.GetMsg());
  msg.speed_profile = speed_prof_factory->Get(speed_profile);
  return msg;
}

void ArcPrimitive::SetMsg(const route_common::ArcPrimitive &msg, const SpeedProfileFactory::Ptr &speed_prof_factory) {
  start = ca::point_cast<Eigen::Vector3d>(msg.start);
  end = ca::point_cast<Eigen::Vector3d>(msg.end);
  points.clear();
  for (auto& it : msg.points) {
    RoutePoint pt;
    pt.SetMsg(it);
    points.push_back(pt);
  }
  speed_profile.reset(speed_prof_factory->Get(msg.speed_profile));
}

route_common::Tunnel Tunnel::GetMsg() const {
  route_common::Tunnel msg;
  msg.margin_left_m = margin_left_m;
  msg.margin_right_m = margin_right_m;
  msg.margin_up_m = margin_up_m;
  msg.margin_down_m = margin_down_m;
  return msg;
}

void Tunnel::SetMsg(const route_common::Tunnel &msg) {
  margin_left_m = msg.margin_left_m;
  margin_right_m = msg.margin_right_m;
  margin_up_m = msg.margin_up_m;
  margin_down_m = msg.margin_down_m;
}

route_common::TouchdownSite TouchdownSite::GetMsg() const {
  route_common::TouchdownSite msg;
  msg.id = id;
  msg.position = ca::point_cast<geometry_msgs::Point>(position);
  msg.heading = heading;
  msg.hover_height = hover_height;
  return msg;
}

void TouchdownSite::SetMsg(const route_common::TouchdownSite & msg) {
  id = msg.id;
  position = ca::point_cast<Eigen::Vector3d>(msg.position);
  heading = msg.heading;
  hover_height = msg.hover_height;
}



route_common::TerminalInvariance TerminalInvariance::GetMsg() const {
  route_common::TerminalInvariance msg;
  msg.type = type;
  msg.segment_id = segment_id;
  msg.loiter_radius = loiter_radius;
  msg.velocity = velocity;
  msg.heading = heading;
  msg.orientation = orientation;
  for (auto& it : touchdown_sites)
    msg.touchdown_sites.push_back(it.GetMsg());
  msg.endcap = endcap.GetMsg();
  return msg;
}

void TerminalInvariance::SetMsg(const route_common::TerminalInvariance & msg) {
  type = msg.type;
  segment_id = msg.segment_id;
  loiter_radius = msg.loiter_radius;
  velocity = msg.velocity;
  heading = msg.heading;
  orientation = msg.orientation;

  touchdown_sites.clear();
  for (auto& it : msg.touchdown_sites) {
    TouchdownSite ts;
    ts.SetMsg(it);
    touchdown_sites.push_back(ts);
  }

  endcap.FromMsg(msg.endcap);
}


std::vector<int> TerminalInvariance::GetIDSet() const {
  if (type == TI_LAND) {
    std::vector<int> id_set;
    for (auto it : touchdown_sites)
      id_set.push_back(it.id);
    return id_set;
  } else {
    return std::vector<int>{0};
  }
}


route_common::RouteMetadata RouteMetadata::GetMsg() const {
  route_common::RouteMetadata msg;
  msg.progress_offset = progress_offset;
  msg.distance_branchoff = distance_branchoff;
  msg.height_branchoff = height_branchoff;
  msg.velocity_branchoff = velocity_branchoff;
  msg.desired_slope = desired_slope;
  msg.approach_direction = approach_direction;
  msg.approach_direction_length = approach_direction_length;
  msg.wind_magnitude = wind_magnitude;
  msg.wind_direction_from = wind_direction_from;
  msg.waveoff_detour = waveoff_detour.GetMsg();
  return msg;
};

void RouteMetadata::SetMsg(const route_common::RouteMetadata & msg) {
  progress_offset = msg.progress_offset;
  distance_branchoff = msg.distance_branchoff;
  height_branchoff = msg.height_branchoff;
  velocity_branchoff = msg.velocity_branchoff;
  desired_slope = msg.desired_slope;
  approach_direction = msg.approach_direction;
  approach_direction_length = msg.approach_direction_length;
  wind_magnitude = msg.wind_magnitude;
  wind_direction_from = msg.wind_direction_from;
  waveoff_detour.SetMsg(msg.waveoff_detour);
}

route_common::RouteSegmentPrimitive RouteSegmentPrimitive::GetMsg(const SpeedProfileFactory::Ptr &speed_prof_factory) const {
  route_common::RouteSegmentPrimitive msg;
  msg.segment_main = segment_main.GetMsg(speed_prof_factory);
  msg.arc_out = arc_out.GetMsg(speed_prof_factory);
  return msg;
}
void RouteSegmentPrimitive::SetMsg(const route_common::RouteSegmentPrimitive & msg, const SpeedProfileFactory::Ptr &speed_prof_factory) {
  segment_main.SetMsg(msg.segment_main, speed_prof_factory);
  arc_out.SetMsg(msg.arc_out, speed_prof_factory);
}

route_common::Route Route::GetMsg() const {
  route_common::Route msg;
  for (auto& it : waypoints)
    msg.waypoints.push_back(it.GetMsg());

  for (auto& it : tunnel)
    msg.tunnel.push_back(it.GetMsg());

  msg.terminal_invariance = terminal_invariance.GetMsg();
  msg.metadata = metadata.GetMsg();
  return msg;
}

void Route::SetMsg(const route_common::Route & msg) {
  waypoints.clear();
  for (auto& it : msg.waypoints) {
    RoutePoint rp;
    rp.SetMsg(it);
    waypoints.push_back(rp);
  }

  tunnel.clear();
  for (auto& it : msg.tunnel) {
    Tunnel tun;
    tun.SetMsg(it);
    tunnel.push_back(tun);
  }

  terminal_invariance.SetMsg(msg.terminal_invariance);
  metadata.SetMsg(msg.metadata);
}

route_common::RouteGuide RouteGuide::GetMsg() const {
  SpeedProfileFactory::Ptr speed_prof_factory(new SpeedProfileFactory());
  route_common::RouteGuide msg;

  msg.route = route.GetMsg();
  for (auto& it : guide)
    msg.guide.push_back(it.GetMsg(speed_prof_factory));
  msg.guide_index = guide_index;
  return msg;
}

void RouteGuide::SetMsg(const route_common::RouteGuide & msg) {
  SpeedProfileFactory::Ptr speed_prof_factory(new SpeedProfileFactory());
  route.SetMsg(msg.route);
  guide.clear();
  for (auto& it : msg.guide) {
    RouteSegmentPrimitive rsp;
    rsp.SetMsg(it, speed_prof_factory);
    guide.push_back(rsp);
  }
  guide_index = msg.guide_index;
}

}  // namespace ca


