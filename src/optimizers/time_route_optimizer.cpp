/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * time_route_optimizer.cpp
 *
 *  Created on: Feb 11, 2015
 *      Author: Sanjiban Choudhury
 */

#include "route_common/optimizers/time_route_optimizer.h"
#include <angles/angles.h>
#include <nlopt.hpp> // C++ interface for nlopt

#include "math_utils/math_utils.h"
#include "speed_profile/common_speed_profiles/uniform_speed_profile.h"
#include "speed_profile/common_speed_profiles/trapezoid_speed_profile.h"
#include "speed_profile/common_speed_profiles/accarray_speed_profile.h"
#include "speed_profile/common_speed_profiles/concatenated_speed_profile.h"

namespace ca {

namespace ang = math_utils::angular_math;
namespace nu = math_utils::numeric_operations;
namespace cnst = math_utils::constants;
namespace vm = math_utils::vector_math;

bool TimeRouteOptimizer::ComputeNewRoute(const Route &input_route, const EndConstraints &end_constraints, RouteGuide &output_route_guide) {
  // Bookkeeping
  //FIXME Cant handle 2 points!!!
  if(input_route.waypoints.size()<=1) {
    ROS_ERROR_STREAM("[time_route_optimizer] Route has not more than 1 way-points");
    return false;
  }
  if(input_route.tunnel.size()<=1) {
    ROS_ERROR_STREAM("[time_route_optimizer] Route has no tunnels");
    return false;
  }
  if(input_route.waypoints.size()!=input_route.tunnel.size()) {
    ROS_ERROR_STREAM("[time_route_optimizer] Route has mismatch between way-point and tunnel");
    return false;
  }
  output_route_guide.route = input_route;
  for(unsigned int i = 1; i< output_route_guide.route.waypoints.size(); i++)
    output_route_guide.route.waypoints[i].heading = atan2(output_route_guide.route.waypoints[i].position.y() - output_route_guide.route.waypoints[i-1].position.y(),
                                                          output_route_guide.route.waypoints[i].position.x() - output_route_guide.route.waypoints[i-1].position.x());

  output_route_guide.route.waypoints[0].heading = output_route_guide.route.waypoints[1].heading;

  // Wind info
  arc_generator_->SetWindData(input_route.metadata.wind_magnitude, input_route.metadata.wind_direction_from);

  // Parse route into opt data
  OptData data;
  data.arc_generator = arc_generator_;
  data.hc = constraints_;
  data.v0 = std::min(input_route.waypoints.front().velocity, constraints_.max_velocity.x());
  data.vf = 0;
  data.segment_set.resize(input_route.waypoints.size()-1);

  for (size_t i = 0; i < input_route.waypoints.size() - 1; i++) {
    SegmentInfo seg;
    seg.length = (input_route.waypoints[i+1].position - input_route.waypoints[i].position).norm();
    seg.heading = atan2(input_route.waypoints[i+1].position.y() - input_route.waypoints[i].position.y(),
                        input_route.waypoints[i+1].position.x() - input_route.waypoints[i].position.x());
    seg.max_vel = std::min(input_route.waypoints[i+1].velocity, constraints_.max_velocity.x());
    seg.tunnel = input_route.tunnel[i+1];

    // shrink tunnel laterally
    seg.tunnel.margin_left_m -= std::min(10.0, seg.tunnel.margin_left_m);
    seg.tunnel.margin_right_m -= std::min(10.0, seg.tunnel.margin_right_m);

    data.segment_set[i] = seg;
  }

  if(!end_constraints.last_point_stop) {
    //For last point only last segment and loiter speed matters if no stop required
    double loiter_vel = output_route_guide.route.terminal_invariance.velocity;
    //Solve the quadratic problem
    //1/(g*tan(phi_max) * v^2  + (phi_max/d_phi_max) * v - lmin = 0
    double qpa, qpb, qpc;
    qpa = 1/(cnst::g*tan(constraints_.max_roll));
    qpb = constraints_.max_roll/constraints_.max_roll_rate;
    qpc = - output_route_guide.route.terminal_invariance.loiter_radius;
    double radius_vel = (-qpb + sqrt(qpb*qpb-4*qpa*qpc))/(2*qpa); //take positive solution (guaranteed to be positive because determinant positive)
    radius_vel = std::numeric_limits<double>::max(); // hack copied from basic optimizer because we wont let loiter radius be a bottle neckl

    data.vf = std::min(std::min(loiter_vel, radius_vel), constraints_.max_velocity.x());
  } else if(end_constraints.last_point_stop) {
    data.vf = 0;
    if (end_constraints.approach_bottle_neck) {
      data.segment_set.back().length -= end_constraints.dist_from_end;
      data.vf = end_constraints.vel_bottle_neck;
    }
  }

  std::vector<double> optim_v;

  if (input_route.waypoints.size() > 2) {
    // Initial Guess
    std::vector<double> p0(input_route.waypoints.size()-2);
    for (size_t i = 0; i < data.segment_set.size()-1; i++)
      p0[i] = 0.1*data.segment_set[i+1].max_vel;

    // Set bounds and constraints
    std::vector<double> lb(input_route.waypoints.size()-2, std::numeric_limits<double>::epsilon());

    std::vector<double> ub(input_route.waypoints.size()-2);
    for (size_t i = 0; i < data.segment_set.size()-1; i++)
      ub[i] = (data.segment_set[i+1].max_vel);

    // Setup optimization
    nlopt::opt opt(nlopt::LN_COBYLA, p0.size());
    opt.set_lower_bounds(lb);
    opt.set_upper_bounds(ub);


    opt.set_min_objective(CostFunction, &data);
    std::vector<double> tol(3*p0.size()+2, 0);
    opt.add_inequality_mconstraint(ConstraintFunction, &data, tol);
    opt.set_maxtime(0.5);
    opt.set_ftol_abs(1e-2);

    // Call Optimizer
    double minf;
    try {
      nlopt::result result = opt.optimize(p0, minf);

      if (result == nlopt::SUCCESS || result == nlopt::FTOL_REACHED) {
        optim_v = p0;
      } else {
        std::cout<<"Failed : "<<minf<<"\n";
        return false;
      }
    }  catch (...) {
      // catched exception from optimization --> return false
      return false;
    }
  }

  // Create route guide
  output_route_guide.guide_index = 0;
  output_route_guide.guide.resize(output_route_guide.route.waypoints.size() - 1);

  output_route_guide.guide.front().segment_main.start = output_route_guide.route.waypoints.front().position;
  output_route_guide.guide.back().segment_main.end = output_route_guide.route.waypoints.back().position;
  output_route_guide.guide.back().arc_out.speed_profile.reset(new UniformSpeedProfile(0));
  // the last arc has no significance

  for(size_t i = 1; i < output_route_guide.route.waypoints.size() - 1; i++) {
    double vel = optim_v[i-1];

    Eigen::Vector3d dir1 = (output_route_guide.route.waypoints[i].position - output_route_guide.route.waypoints[i-1].position);
    if (dir1.norm() > 0 ) dir1.normalize();
    Eigen::Vector3d dir2 = (output_route_guide.route.waypoints[i+1].position - output_route_guide.route.waypoints[i].position);
    if (dir2.norm() > 0 ) dir2.normalize();

    ArcGenerator::Arc arc;
    double l1, l2;
    arc_generator_->GetArc(vel, output_route_guide.route.waypoints[i].heading, output_route_guide.route.waypoints[i+1].heading, arc, l1, l2);

    ArcPrimitive turn;
    turn.start = output_route_guide.route.waypoints[i].position - l1*dir1;
    turn.end = output_route_guide.route.waypoints[i].position + l2*dir2;
    //urn.end[2] = turn.start[2]; // make turns flat

    for (auto it : arc) {
      RoutePoint pt;
      pt.position = Eigen::Vector3d(it.x(), it.y(), 0);
      pt.position = vm::Rotate2DByAngle(pt.position, output_route_guide.route.waypoints[i].heading) + output_route_guide.route.waypoints[i].position;
      pt.position[2] = turn.start[2];
      pt.velocity = vel;
      turn.points.push_back(pt);
    }

    turn.speed_profile.reset(new UniformSpeedProfile(vel));

    //i-1 arc out
    output_route_guide.guide[i-1].arc_out = turn;

    //i-1 end segment location
    output_route_guide.guide[i-1].segment_main.end = turn.start;
    //i start segment location
    output_route_guide.guide[i].segment_main.start = turn.end;
  }


  for(unsigned int i = 0; i < output_route_guide.guide.size(); i++) {
    if (i == output_route_guide.guide.size() - 1 && end_constraints.approach_bottle_neck && end_constraints.last_point_stop) {
      double l = (output_route_guide.guide[i].segment_main.end - output_route_guide.guide[i].segment_main.start).norm();
      l -= end_constraints.dist_from_end;
      double v1 = (i==0) ? data.v0 : optim_v[i-1];
      double v2 = end_constraints.vel_bottle_neck;
      double vel = GetSegmentVel(v1, v2, data.segment_set[i].max_vel, l, constraints_);


      double a1 = vel < v1 ? -constraints_.max_accel[0] : constraints_.max_accel[0]; //a1 has to be positive always
      double a3 = vel < v2 ? constraints_.max_accel[0] : -constraints_.max_accel[0];

      TrapezoidSpeedProfile trap_spf(v1,vel,v2,a1,a3);
      boost::shared_ptr<AccArraySpeedProfile> acc_spf = boost::dynamic_pointer_cast<AccArraySpeedProfile>(end_constraints.approach_profile);

      output_route_guide.guide[i].segment_main.speed_profile.reset(new ConcatenatedSpeedProfile(trap_spf, *acc_spf));
      output_route_guide.route.waypoints[i+1].velocity = vel;
    } else {
      double l = (output_route_guide.guide[i].segment_main.start - output_route_guide.guide[i].segment_main.end).norm();
      double v1 = (i==0) ? data.v0 : optim_v[i-1];
      double v3 = (i == output_route_guide.guide.size() - 1) ? data.vf : optim_v[i];
      double vel = GetSegmentVel(v1, v3, data.segment_set[i].max_vel, l, constraints_);

      double a1 = vel < v1 ? -constraints_.max_accel[0] : constraints_.max_accel[0]; //a1 has to be positive always
      double a3 = vel < v3 ? constraints_.max_accel[0] : -constraints_.max_accel[0];
      output_route_guide.guide[i].segment_main.speed_profile.reset(new TrapezoidSpeedProfile(v1, vel, v3, a1, a3));
      output_route_guide.route.waypoints[i+1].velocity = vel;
    }
  }

  // Copied below section from route optimizer
  if(output_route_guide.route.terminal_invariance.type == TerminalInvariance::TI_LOITER) {
    double vel = data.vf;
    double radius = output_route_guide.route.terminal_invariance.loiter_radius;  // (FIXME) this doesnt seem right
    //Correct loiter radius
    if(radius < (vel*vel)/(cnst::g*tan(0.9*constraints_.max_roll)) +  vel*constraints_.max_roll/constraints_.max_roll_rate)
      radius = (vel*vel)/(cnst::g*tan(0.9*constraints_.max_roll)) + vel* constraints_.max_roll/constraints_.max_roll_rate;
    output_route_guide.route.terminal_invariance.loiter_radius = radius;
   //output_route_guide.route.terminal_invariance.velocity = output_route_guide.route.waypoints.back().velocity;
  }
  return true;
}

double TimeRouteOptimizer::CostFunction(const std::vector<double> &p, std::vector<double> &grad, void *f_data) {
  OptData *data = static_cast<OptData*> (f_data);
  std::vector<std::pair<double, double> > length_buffer(data->segment_set.size());
  double time = 0;

  length_buffer[0].first = 0;
  length_buffer.back().second = 0;
  for (size_t i = 0; i < p.size(); i++) {
    double vel = std::max(p[i], std::numeric_limits<double>::epsilon());
    ArcGenerator::Arc arc;
    double l1, l2;
    data->arc_generator->GetArc(vel, data->segment_set[i].heading,  data->segment_set[i+1].heading, arc, l1, l2);
    double arc_length = 0;
    for (size_t j = 0; j < arc.size() - 1; j++)
      arc_length += (arc[j+1] - arc[j]).norm();
    time += arc_length / vel;
    length_buffer[i].second = l1;
    length_buffer[i+1].first = l2;
  }

  std::vector<double> buffered_vel;
  buffered_vel.push_back(data->v0);
  buffered_vel.insert(buffered_vel.end(), p.begin(), p.end());
  buffered_vel.push_back(data->vf);

  for (size_t i = 0; i < data->segment_set.size(); i++) {
    time = time + GetSegmentTime( buffered_vel[i], buffered_vel[i+1], data->segment_set[i].max_vel,
                                  data->segment_set[i].length - length_buffer[i].first - length_buffer[i].second, data->hc);
  }

  return time;
}

void TimeRouteOptimizer::ConstraintFunction(unsigned m, double *result, unsigned n, const double* p, double* grad, void* f_data) {
  OptData *data = static_cast<OptData*> (f_data);
  std::vector<std::pair<double, double> > length_buffer(data->segment_set.size());
  double time = 0;

  length_buffer[0].first = 0;
  length_buffer.back().second = 0;
  for (size_t i = 0; i < n; i++) {
    double vel = std::max(p[i], std::numeric_limits<double>::epsilon());
    ArcGenerator::Arc arc;
    double l1, l2;
    data->arc_generator->GetArc(vel, data->segment_set[i].heading,  data->segment_set[i+1].heading, arc, l1, l2);
    result[i] = data->arc_generator->ArcTunnelViolation(arc, data->segment_set[i].tunnel, data->segment_set[i+1].tunnel,
                                                        angles::normalize_angle(data->segment_set[i+1].heading - data->segment_set[i].heading));
    double arc_length = 0;
    for (size_t j = 0; j < arc.size() - 1; j++)
      arc_length += (arc[j+1] - arc[j]).norm();
    time += arc_length / vel;
    length_buffer[i].second = l1;
    length_buffer[i+1].first = l2;
  }

  std::vector<double> buffered_vel;
  buffered_vel.push_back(data->v0);
  for (size_t i = 0; i < n; i++) {
    buffered_vel.push_back(p[i]);
  }
  buffered_vel.push_back(data->vf);

  int counter = n;
  for (size_t i = 0; i < data->segment_set.size(); i++) {
    result[counter++] = length_buffer[i].first + length_buffer[i].second - data->segment_set[i].length;
    double acc_length = std::max(std::numeric_limits<double>::epsilon(), data->segment_set[i].length - length_buffer[i].first - length_buffer[i].second);
    result[counter++] = std::abs((buffered_vel[i]*buffered_vel[i] - buffered_vel[i+1]*buffered_vel[i+1])/(2.0*acc_length)) - data->hc.max_accel.x();
  }
}

double TimeRouteOptimizer::GetSegmentTime(double v1, double v2, double vseg, double length, const UAVDynamicsConstraints &hc) {
  v1 = std::max(v1, 0.0);
  v2 = std::max(v2, 0.0);
  length = std::max(length, std::abs( (v2*v2 - v1*v1)/(2.0*hc.max_accel.x()) ));
  vseg = std::max(vseg, std::max(v2, v2));
  double vlim = sqrt(0.5*(v1*v1 + v2*v2) + hc.max_accel.x()*length);
  if (vseg < vlim) {
    double s1 = (vseg*vseg - v1*v1)/(2.0*hc.max_accel.x());
    double s2 = (vseg*vseg - v2*v2)/(2.0*hc.max_accel.x());
    return (vseg - v1)/hc.max_accel.x() + (vseg-v2)/hc.max_accel.x()+ (length - s1 - s2)/vseg;
  } else {
    return (vlim-v1)/hc.max_accel.x() + (vlim-v2)/hc.max_accel.x();
  }
}



double TimeRouteOptimizer::GetSegmentVel(double v1, double v2, double vseg, double length, const UAVDynamicsConstraints &hc) {
  v1 = std::max(v1, 0.0);
  v2 = std::max(v2, 0.0);
  length = std::max(length, std::abs( (v2*v2 - v1*v1)/(2.0*hc.max_accel.x()) ));
  vseg = std::max(vseg, std::max(v2, v2));
  double vlim = sqrt(0.5*(v1*v1 + v2*v2) + hc.max_accel.x()*length);
  return std::min(vseg, vlim);
}




}  // namespace ca


