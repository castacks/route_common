/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * trochoid_arc_generator.cpp
 *
 *  Created on: Jun 8, 2015
 *      Author: Sanjiban Choudhury
 */

#include "route_common/arc_generators/trochoid_arc_generator.h"
#include <boost/math/constants/constants.hpp>
#include <angles/angles.h>

namespace bm = boost::math::constants;

namespace ca {

void TrochoidArcGenerator::GetArc(double vel, double seg1_heading, double seg2_heading, Arc &arc, double &l1, double &l2) {
  double angle = angles::normalize_angle(seg2_heading - seg1_heading);
  vel = std::max(vel, wind_speed_*1.5);

  arc.emplace_back(0,0);
  int num_seg = 50;
  for (int i = 0; i <= num_seg; i++) {
    double theta = (((double)i)/num_seg)*angle;
    double rel_wind_dir = angles::normalize_angle_positive(seg1_heading + theta - wind_direction_);
    double curv_max =  GetMaxCurveTrochoid(rel_wind_dir, vel, wind_speed_, GetTurnRate(hc_.max_roll, vel));
    double delta_s = std::abs(angle/(double)num_seg)/curv_max;
    Eigen::Vector2d new_pt = arc.back() + Eigen::Vector2d(cos(theta), sin(theta))*delta_s;
    arc.push_back(new_pt);
  }

  l2 = arc.back().y()/sin(angle);
  l1 = arc.back().x() - l2*cos(angle);

  for (auto &it : arc)
    it[0] -= l1;
}

double TrochoidArcGenerator::GetMaxCurveTrochoid(double rel_heading, double heli_speed, double wind_speed, double heli_turnrate) {
  double t3 = cos(rel_heading);
  return heli_speed * heli_turnrate * (heli_speed+wind_speed * t3) * 1.0/pow((heli_speed*heli_speed+wind_speed*wind_speed+heli_speed*wind_speed*t3*2.0),(3.0/2.0));
}

double TrochoidArcGenerator::GetTurnRate(double bank_angle, double heli_speed) {
  const double radius = (heli_speed*heli_speed)/(9.81*tan(bank_angle));
  return heli_speed / radius;
}

}  // namespace ca



