/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * circular_arc_generator.cpp
 *
 *  Created on: Feb 11, 2015
 *      Author: Sanjiban Choudhury
 */

#include "route_common/arc_generators/circular_arc_generator.h"
#include <boost/math/constants/constants.hpp>
#include <angles/angles.h>

namespace bm = boost::math::constants;

namespace ca {

void CircularArcGenerator::GetArc(double vel, double seg1_heading, double seg2_heading, Arc &arc, double &l1, double &l2) {
  //double radius = (vel*vel)/(9.81 * tan(hc_.max_roll));
  double radius = (vel*vel)/(9.81 * tan(hc_.max_roll)) + (1.0/bm::pi<double>())*vel*(hc_.max_roll/hc_.max_roll_rate);
  double angle = angles::normalize_angle(seg2_heading - seg1_heading);
  double l = radius*tan(0.5*angle);
  l1 = l;
  l2 = l;
  for (int i = 0; i <= 30; i++) {
    double gamma = -bm::pi<double>()/2 + (((double)i)/30.0)*angle;
    arc.emplace_back(-l + radius*cos(gamma), radius + radius*sin(gamma));
  }
}


}  // namespace ca


