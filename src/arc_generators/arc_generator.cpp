/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * arc_generator.cpp
 *
 *  Created on: Feb 11, 2015
 *      Author: Sanjiban Choudhury
 */

#include "route_common/arc_generators/arc_generator.h"

namespace ca {

bool ArcGenerator::IsArcInTunnel(const Arc &arc, const Tunnel &tunnel1, const Tunnel &tunnel2, double angle) {
  double w1, w2;
  if (angle >= 0) {
    w1 = tunnel1.margin_right_m;
    w2 = tunnel2.margin_right_m;
  } else {
    w1 = tunnel1.margin_left_m;
    w2 = tunnel2.margin_left_m;
  }
  Eigen::Vector2d l1(-1, 0), l2(cos(angle), sin(angle));
  for (auto it : arc) {
    if ( !IsInBox(it, l1, w1) && !IsInBox(it, l2, w2) )
      return false;
  }
  return true;
}

bool ArcGenerator::IsInBox(const Eigen::Vector2d &p, const Eigen::Vector2d &l, double width) {
  Eigen::Vector2d offset = p - (l.dot(p))*l;
  return (l.dot(p) > 0) && offset.norm() < width;
}

double ArcGenerator::AmountInBox(const Eigen::Vector2d &p, const Eigen::Vector2d &l, double width) {
  Eigen::Vector2d offset = p - (l.dot(p))*l;
  return (l.dot(p) > 0)*(offset.norm() - width);
}



double ArcGenerator::ArcTunnelViolation(const Arc &arc, const Tunnel &tunnel1, const Tunnel &tunnel2, double angle) {
  double w1, w2;
  if (angle >= 0) {
    w1 = tunnel1.margin_right_m;
    w2 = tunnel2.margin_right_m;
  } else {
    w1 = tunnel1.margin_left_m;
    w2 = tunnel2.margin_left_m;
  }
  Eigen::Vector2d l1(-1, 0), l2(cos(angle), sin(angle));
  double max_viol = -std::numeric_limits<double>::max();
  for (auto it : arc) {
    max_viol = std::max(max_viol, std::min(AmountInBox(it, l1, w1), AmountInBox(it, l2, w2)));
  }
  return max_viol;
}


}  // namespace ca


